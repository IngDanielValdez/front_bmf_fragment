import { Component } from '@angular/core';
import { ConfigService } from './providers/config.service';
import { AuthService } from './providers/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  constructor(public config: ConfigService, public auth: AuthService) {
  }
}
