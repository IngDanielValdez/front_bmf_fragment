import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhonePipe } from './pipes/phone.pipe';
import { PaginationComponent } from './components/pagination/pagination.component';
import { InfoboxComponent } from './components/infobox/infobox.component';



@NgModule({
  declarations: [PhonePipe, PaginationComponent, InfoboxComponent],
  imports: [
    CommonModule
  ],
  exports: [PhonePipe, PaginationComponent, InfoboxComponent]
})
export class MainModule { }
