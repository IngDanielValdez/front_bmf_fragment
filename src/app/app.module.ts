import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MenuComponent } from './components/menu/menu.component';
import { FooterComponent } from './components/footer/footer.component';
import { BanksModule } from './modules/banks/banks.module';
import { RouterModule } from '@angular/router';
import { Routing } from './app.routing';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { HttpClientModule } from '@angular/common/http';
import { UsersModule } from './modules/users/users.module';
import { CompaniesModule } from './modules/companies/companies.module';
import { AuthModule } from './modules/auth/auth.module';
import { DealsModule } from './modules/deals/deals.module';
import { HallofFameModule } from './modules/hallof-fame/hallof-fame.module';
import { PostmanModule } from './modules/postman/postman.module';
import { CalendarModule } from './modules/calendar/calendar.module';
import { MainModule } from './main/main.module';
import { BanklistsModule } from './modules/banklists/banklists.module';
import { AdmreportModule } from './modules/admreport/admreport.module';
import { ApplicationsModule } from './modules/applications/applications.module';
import { ActivitiesModule } from './modules/activities/activities.module';
import { ContactListModule } from './modules/contact-list/contact-list.module';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MenuComponent,
    FooterComponent,
    DashboardComponent
  ],
  imports: [
    RouterModule.forRoot(Routing),
    HttpClientModule,
    BrowserModule,
    BanksModule,
    UsersModule,
    CompaniesModule,
    AuthModule,
    DealsModule,
    HallofFameModule,
    PostmanModule,
    CalendarModule,
    ChartsModule,
    WavesModule,
    MainModule,
    BanklistsModule,
    AdmreportModule,
    ApplicationsModule,
    ActivitiesModule,
    ContactListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
