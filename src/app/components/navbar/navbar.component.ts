import { Component, ViewChild, ElementRef, AfterViewInit, OnInit, AfterViewChecked, ChangeDetectorRef } from '@angular/core';
import { ConfigService } from 'src/app/providers/config.service';
import { AuthService } from 'src/app/providers/auth.service';
import { User } from 'src/app/modules/users/interfaces/user.interface';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styles: []
})
export class NavbarComponent implements OnInit {
  url = environment.APIURI;
  constructor(public config: ConfigService, public auth: AuthService, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.cdr.detectChanges();
    this.config.hideMenu = window.innerWidth <= 1024 ? true : false;
  }

  checkWidth(event) {
    this.config.hideMenu = event.target.innerWidth <= 1024 ? true : false;
  }

  prepairPhoto(user: User) {
    if (user.photo) {
      const folder = user.photo.split('/');
      const file = folder.pop();
      return `users/file/download/?path=${ folder.join('/') }&file=${ file }&at=${ this.auth.returnToken() }`;
    }
  }


}
