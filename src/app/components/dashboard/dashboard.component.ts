import { Component, OnInit } from '@angular/core';
import { StatsService } from 'src/app/providers/stats.service';
import { AuthService } from 'src/app/providers/auth.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {
  stats;
  info;
  url = environment.APIURI;
  constructor(private statsAPI: StatsService, private auth: AuthService) { }
  public chartType = 'line';

  public chartDatasets: Array<any> = [
    { data: [0, 0, 0, 5], label: 'Monthly Earnings' },
  ];

  public chartLabels: Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  public chartColors: Array<any> = [
    {
      backgroundColor: 'rgb(48, 166, 211, .2)',
      borderColor: 'rgb(48, 166, 211)',
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {
    responsive: true
  };

  ngOnInit() {
    this.getStats();
  }

  getStats() {
    this.statsAPI.getInitStats()
    .subscribe((resp) => {
      this.stats = resp;
    }, (resp) => {
      console.log(resp);
    });
  }

  prepairPhoto(employee) {
    if (employee.photo) {
      const folder = employee.photo.split('/');
      const file = folder.pop();
      return `featured-employee/file/download/?path=${ folder.join('/') }&file=${ file }&at=${ this.auth.returnToken() }`;
    }
  }
}
