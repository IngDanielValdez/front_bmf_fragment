import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, AbstractControl } from '@angular/forms';
import * as moment from 'moment';
import { CompanyService } from 'src/app/providers/company.service';
import { Company } from '../interfaces/company.interface';
import { Router, ActivatedRoute } from '@angular/router';
import { Statement } from '../interfaces/statement.interface';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})
export class EditComponent implements OnInit {
  step = 1;
  tmpFolder;
  currentDate = new Date();
  companyForm: FormGroup;
  period = `${ new Date().getMonth() + 1 }-${new Date().getFullYear() }`;
  infoFile;
  sendingFile;
  loading = true;
  sending = false;
  info;
  company;
  constructor(private fb: FormBuilder, private companyAPI: CompanyService, private router: Router, private activeRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activeRoute.params.subscribe(params => this.getCompany(params.id));
  }

  getCompany(id) {
    this.companyAPI.getCompany(id)
    .subscribe((resp: any) => {
      this.tmpFolder = resp.code;
      this.company = resp;
      this.createForm(resp);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  sendCompany() {
    this.sending = true;
    this.companyAPI.updateCompany(this.companyForm.value, this.company._id)
    .subscribe((resp: Company) => {
      this.sending = false;
      this.router.navigate(['companies/details', resp._id]);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  getMonth(substract) {
    return moment().month(this.currentDate.getMonth() - substract).format('MMMM');
  }

  isActiveInBK(obj, key) {
    return obj !== null && obj !== undefined && key in obj ? true : false;
  }

  createForm(form?) {
    this.companyForm = this.fb.group({
      name: new FormControl(this.isActiveInBK(form, 'name') ? form.name : '',
      [Validators.required, Validators.maxLength(240), Validators.minLength(3)]),
      dba: new FormControl(this.isActiveInBK(form, 'dba') ? form.dba : '',
      [Validators.required, Validators.maxLength(240), Validators.minLength(1)]),
      service: new FormControl(this.isActiveInBK(form, 'service') ? form.service : '',
      [Validators.required, Validators.min(0), Validators.max(240)]),
      address: new FormControl(this.isActiveInBK(form, 'address') ? form.address : '',
      [Validators.required, Validators.maxLength(240)]),
      city: new FormControl(this.isActiveInBK(form, 'city') ? form.city : '',
      [Validators.required, Validators.maxLength(80)]),
      state: new FormControl(this.isActiveInBK(form, 'state') ? form.state : '',
      [Validators.required, Validators.maxLength(80)]),
      zip: new FormControl(this.isActiveInBK(form, 'zip') ? form.zip : '',
      [Validators.required, Validators.maxLength(30), Validators.pattern(/[0-9]+/)]),
      phone: this.fb.array([]),
      email: this.fb.array([]),
      cardprocessor: new FormControl(this.isActiveInBK(form, 'cardprocessor') ? form.cardprocessor : false),
      otherloans: new FormControl(this.isActiveInBK(form, 'otherloans') ? form.otherloans : false),
      otheraccounts: new FormControl(this.isActiveInBK(form, 'otheraccounts') ? form.otheraccounts : false),
      members: this.fb.array([]),
      bankStatements: this.fb.array([]),
      cardStatements: this.fb.array([]),
      additionalAccounts: this.fb.array([]),
      loans: this.fb.array([]),
      code: new FormControl(this.isActiveInBK(form, 'code') ? form.code : this.tmpFolder, Validators.required),
      app: new FormControl(this.isActiveInBK(form, 'app') ? form.app : '', [Validators.required, Validators.minLength(5)]),
    });

    this.isActiveInBK(form, 'members') ? form.members.forEach((member) => this.addMembers(member)) :
    this.addMembers();

    this.isActiveInBK(form, 'phone') && Array.isArray(form.phone) ? form.phone.forEach(ph => this.increaseArray('phone', ph)) :
    this.increaseArray('phone');

    this.isActiveInBK(form, 'email') && Array.isArray(form.email) ? form.email.forEach(mail => this.increaseArray('email', mail)) :
    this.increaseArray('email');

    if (this.isActiveInBK(form, 'cardprocessor') && form.cardprocessor) {
      if (this.isActiveInBK(form, 'cardStatements') && Array.isArray(form.cardStatements)
      && form.cardStatements.filter(statement => statement.period === this.period).length === 3) {
        form.cardStatements.forEach(statement => {
          if (statement.period === this.period) {
            this.setStatements('cardStatements', statement);
          }
        });
      } else {
        this.setStatements('cardStatements');
        this.setStatements('cardStatements');
        this.setStatements('cardStatements');
      }
    }

    if (this.isActiveInBK(form, 'otheraccounts') && form.otheraccounts) {
      if (this.isActiveInBK(form, 'additionalAccounts') && Array.isArray(form.additionalAccounts)
      && form.additionalAccounts.filter(statement => statement.period === this.period).length === 3) {
          form.additionalAccounts.forEach(statement => {
            if (statement.period === this.period) {
              this.setStatements('additionalAccounts', statement);
            }
          });
      } else {
        this.setStatements('additionalAccounts');
        this.setStatements('additionalAccounts');
        this.setStatements('additionalAccounts');
      }
    }


    if (this.isActiveInBK(form, 'loans') && Array.isArray(form.loans)) {
      form.loans.forEach(loan => this.addLoan(loan));
    }

    if (this.isActiveInBK(form, 'bankStatements')
    && Array.isArray(form.bankStatements)
    && form.bankStatements.filter(statement => statement.period === this.period).length >= 3) {
      form.bankStatements.forEach((statement: Statement) => {
          if (statement.period === this.period) {
            this.setStatements('bankStatements', statement);
          }
        });
    } else {
      // Set 3 Statements
      this.setStatements('bankStatements');
      this.setStatements('bankStatements');
      this.setStatements('bankStatements');
    }

    this.loading = false;
  }

  setStatements(input, obj?) {
    const statement = (this.companyForm.controls[input] as FormArray);
    statement.push(this.fb.group({
      quantity: new FormControl(this.isActiveInBK(obj, 'quantity') ? obj.quantity : '', [Validators.required, Validators.minLength(0)]),
      name: new FormControl(this.isActiveInBK(obj, 'name') ? obj.name : '', [Validators.required, Validators.minLength(5)]),
      period: new FormControl(this.period, Validators.required) }));
  }

  increaseArray(input, value?) {
    const validators = [Validators.required];
    if (input === 'email') { validators.push(Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)); }
    if (input === 'phone') { validators.push(Validators.pattern(/[0-9]/), Validators.minLength(10), Validators.maxLength(15)); }
    (this.companyForm.controls[input] as FormArray).push(new FormControl(value || '', validators));
  }

  forceRemoveInArray(input, index) {
    (this.companyForm.controls[input] as FormArray).removeAt(index);
  }

  removeInArray(input, index) {
    if ((this.companyForm.controls[input] as FormArray).controls.length > 1) {
      (this.companyForm.controls[input] as FormArray).removeAt(index);
    }
  }

  removeInNestedArray(index, subindex, input, control) {
    const nested = (this.companyForm.get(control) as FormArray).controls[index].get(input) as FormArray;
    if (nested.controls.length > 1) { nested.removeAt(subindex); }
  }

  addMembers(member?) {
    let options = {
      name: '', title: '', ownership: '', address: '', city: '', state: '', zip: '', ssn: '', birthdate: '',
      email: [new FormControl('', [Validators.required, Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)])],
      phone: [new FormControl('', [Validators.required, Validators.pattern(/[0-9]/), Validators.minLength(10), Validators.maxLength(15)])]
    };
    if (member) { options = member; }
    let birthdate = '';
    if (options.birthdate) { birthdate = moment(options.birthdate).add(1, 'days').format('YYYY-MM-DD'); }
    (this.companyForm.controls.members as FormArray).push(
      this.fb.group({
      name: new FormControl((options.name), [Validators.required, Validators.maxLength(120), Validators.minLength(3)]),
      title: new FormControl(options.title, [Validators.required, Validators.maxLength(60), Validators.minLength(2)]),
      ownership: new FormControl(options.ownership || '', [Validators.required, Validators.min(0), Validators.max(100)]),
      address: new FormControl(options.address, [Validators.required, Validators.maxLength(80)]),
      city: new FormControl(options.city, [Validators.required, Validators.maxLength(50)]),
      state: new FormControl(options.state, [Validators.required, Validators.maxLength(15)]),
      zip: new FormControl(options.zip, [Validators.required, Validators.maxLength(9), Validators.pattern(/[0-9]+/)]),
      ssn: new FormControl(options.ssn, [Validators.required, Validators.maxLength(16)]),
      birthdate: new FormControl(birthdate, [Validators.required]),
      phone: this.fb.array(options.phone || []),
      email: this.fb.array(options.email || [])
    }));
  }

  addLoan(value?) {
    (this.companyForm.controls.loans as FormArray).push(
      this.fb.group({
      amount: new FormControl(this.isActiveInBK(value, 'amount') ? value.amount : '',
      [Validators.required, Validators.min(0)]),
      currentdebt: new FormControl(this.isActiveInBK(value, 'currentdebt') ? value.currentdebt : '',
      [Validators.required, Validators.min(0)]),
      recurrentpayment: new FormControl(this.isActiveInBK(value, 'recurrentpayment') ? value.recurrentpayment : '',
      [Validators.required, Validators.min(0)]),
      paymenttype: new FormControl(this.isActiveInBK(value, 'paymenttype') ? value.paymenttype : 'daily', [Validators.required]),
    }));
  }

  getArray(input) { return (this.companyForm.get(input) as FormArray).controls ; }

  getArrayNested(index, control, input) {
    return ((this.companyForm.get(control) as FormArray).controls[index].get(input) as FormArray).controls;
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  isInvalidInputFile(input: FormControl) {
    return input.invalid ? true : false;
  }

  increaseArrayInNestedArray(index, input, control) {
    const validators = [Validators.required];
    if (input === 'email') { validators.push(Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)); }
    if (input === 'phone') { validators.push(Validators.pattern(/[0-9]/), Validators.minLength(10), Validators.maxLength(15)); }
    ((this.companyForm.get(control) as FormArray).controls[index].get(input) as FormArray).push(new FormControl('', validators));
  }

  getInput(input) {
    return this.companyForm.get(input) as FormControl;
  }

  toggleClass(input) {
    let type;
    if (input === 'cardprocessor') { type = 'cardStatements'; }
    if (input === 'otheraccounts') { type = 'additionalAccounts'; }
    const statement = (this.companyForm.get(type) as FormArray);
    (this.companyForm.get(input) as FormControl).setValue(!this.companyForm.get(input).value);
    if (!this.companyForm.get(input).value) {
      while (statement.length !== 0) { statement.removeAt(0); }
    } else {
      this.setStatements(type);
      this.setStatements(type);
      this.setStatements(type);
    }
  }

  getFile(event, input: FormControl, folder) {
    if (event.target.files.length > 0) {
      $('#loadingModal').modal('show');

      const file = event.target.files[0];
      const formData = new FormData();
      formData.append('file', file, (file as any).name);

      this.infoFile = {};
      this.sendingFile = true;

      this.companyAPI.updateFile(this.tmpFolder, { subfolder: folder }, formData)
      .subscribe((resp: any) => {
        this.sendingFile = false;
        this.infoFile = { show: true, message: 'File uploaded successfully', class: 'alert alert-success', persist: true };
        input.setValue(resp.key);
      }, (resp) => {
        this.sendingFile = false;
        const content = errorHandler(resp);
        this.infoFile = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
      });
    }
  }

}
