import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './all/all.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { DetailComponent } from './detail/detail.component';
import { AuthGuard } from '../auth/auth.guard';
import { BlacklistComponent } from './blacklist/blacklist.component';

export const companiesRoutes: Routes = [
    { path: 'companies', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'blacklist', component: BlacklistComponent },
        { path: 'add', component: CreateComponent },
        { path: 'edit/:id', component: EditComponent  },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
