import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { CreateComponent } from './create/create.component';
import { DetailComponent } from './detail/detail.component';
import { EditComponent } from './edit/edit.component';
import { AllComponent } from './all/all.component';
import { RouterModule } from '@angular/router';
import { MainModule } from 'src/app/main/main.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { companiesRoutes } from './app.routing';

@NgModule({
  declarations: [AppComponent, CreateComponent, DetailComponent, EditComponent, AllComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(companiesRoutes),
    MainModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class CompaniesModule { }
