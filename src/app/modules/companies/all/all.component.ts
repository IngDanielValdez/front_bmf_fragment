import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/providers/company.service';
import { Company } from '../interfaces/company.interface';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  companies: Company[] = [];
  loading = true;
  total;
  info;
  filters = { name: '', dba: '', taxid: '', service: '', code: '', active: true, skip: 0, limit: 10 };
  constructor(private companyAPI: CompanyService, private activeRoute: ActivatedRoute, private router: Router, private auth: AuthService) {}

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((query) => {
      this.filters.skip = Number(query.skip) || 0;
      this.getCompanies();
    });
  }

  getCompanies() {
    this.loading = true;
    this.companyAPI.getCompanies(this.filters)
    .subscribe((resp: { companies: Company[], total: number }) => {
      this.loading = false;
      this.companies = resp.companies;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getCompanies();
  }

  authRole(role: string[]) {
    return role.indexOf(this.auth.getUserInfo().user.role) !== -1 ? true : false;
  }

}
