import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/providers/company.service';
import { Company } from '../interfaces/company.interface';
import { ActivatedRoute, Router } from '@angular/router';
import { Statement } from '../interfaces/statement.interface';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { DealService } from 'src/app/providers/deal.service';
import * as moment from 'moment';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  tab = 'company';
  company: Company;
  loading = true;
  uploading = false;
  deleting = false;
  downloading = false;
  loadingAttachments = false;
  sendingDeal = false;
  updating = false;
  info;
  infoFile;
  infoAttachments;
  infoDeal;
  periods: Set<string>;
  attachments = [];
  loadingFile;
  deletingFile;
  getAttachmentsObserv: any;
  uploadFileObserv: any;
  deleteFileObserv: any;
  dealForm: FormGroup;
  sources = ['Google', 'Facebook', 'Instagram', 'Linkedin', 'Cold Call', 'Email'];
  constructor(private fb: FormBuilder, private companyAPI: CompanyService, private activeRoute: ActivatedRoute,
              private dealAPI: DealService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getCompany(params.id));
  }

  getCompany(id) {
    this.loading = true;
    this.companyAPI.getCompany(id).subscribe((company: Company) => {
      this.company = company;
      this.loading = false;
      const statements = [];
      statements.push(...this.company.bankStatements);
      statements.push(...this.company.cardStatements);
      statements.push(...this.company.additionalAccounts);
      const filterStatement = statements.map(period => period.period);
      this.periods = new Set(filterStatement);
      this.getAttachments();
      this.createForm();
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  createForm() {
    this.dealForm = this.fb.group({
      companyID: new FormControl(this.company._id, Validators.required),
      quantity: new FormControl('', [Validators.required, Validators.min(1000)]),
      territories: this.fb.array([]),
      source: new FormControl('', [Validators.maxLength(100), Validators.required]),
     });
  }


  sendDeal() {
    this.sendingDeal = true;
    this.dealAPI.createDeal(this.dealForm.value)
    .subscribe((resp: any) => {
      this.sendingDeal = false;
      const message = 'Deal sent successfully';
      this.infoDeal = { show: true, message, class: 'alert alert-success', persist: true };
    }, (resp) => {
      this.sendingDeal = false;
      const content = errorHandler(resp);
      this.infoDeal = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  sendToBlacklist() {
    this.updating = true;
    this.companyAPI.updateCompany({ blacklist: true } as any, this.company._id)
    .subscribe((resp: any) => {
      this.updating = false;
      this.company = resp;
    }, (resp) => {
      this.updating = false;
      const content = errorHandler(resp);
      this.infoDeal = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changeTerritory(territory) {
    const form = (this.dealForm.get('territories') as FormArray);
    const pos = form.value.indexOf(territory);
    pos === -1 ? form.push(new FormControl(territory)) : form.removeAt(pos);
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  getInput(input) {
    return this.dealForm.get(input) as FormControl;
  }

  filterStatement( statements: Statement[], period: string) {
    return statements.filter(statement => statement.period === period);
  }

  shortName(name) {
    return name.split('/').pop();
  }

  downloadFile(path) {
    this.infoFile = '';
    const folder = path.split('/');
    const file = folder.pop();
    this.downloading = true;
    $('#loadingModal').modal('show');
    this.companyAPI.downloadFile(folder.join('/'), file).subscribe(resp => {
      const url = window.URL.createObjectURL(resp);
      const newWindow = window.open(url);
      this.downloading = false;
      let message = 'Download complete, the file will open in a new tab';
      if (!newWindow || newWindow.closed || typeof newWindow.closed === 'undefined') {
        message = 'Please disable your Pop-up blocker and try again';
      }
      this.infoFile = { show: true, message, class: 'alert alert-success', persist: true };
    }, (resp) => {
      this.downloading = false;
      const content = errorHandler(resp);
      this.infoFile = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  setReferral(value) {
    const referr = this.dealForm.get('source');
    referr.setValue(value);
  }

  getAttachments() {
    this.loadingAttachments = true;
    this.infoAttachments = '';
    if (this.getAttachmentsObserv) { this.getAttachmentsObserv.unsubscribe(); }
    this.getAttachmentsObserv = this.companyAPI.getAttachments(this.company.code)
    .subscribe((resp: []) => {
      this.loadingAttachments = false;
      if (resp.length === 0) {
        this.infoAttachments = { show: true, message: 'Nothing to show', class: 'alert alert-success', persist: true };
      }
      this.attachments = resp;
    }, (resp) => {
      this.loadingAttachments = false;
      const content = errorHandler(resp);
      this.infoAttachments = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  uploadFile(event, folder) {
    if (event.target.files.length > 0) {
      $('#uploadModal').modal('show');

      const file = event.target.files[0];
      const formData = new FormData();
      formData.append('file', file, (file as any).name);

      this.loadingFile = {};
      this.uploading = true;
      if (this.uploadFileObserv) { this.uploadFileObserv.unsubscribe(); }
      this.uploadFileObserv = this.companyAPI.updateFile(this.company.code, { subfolder: folder }, formData)
      .subscribe((resp: any) => {
        this.uploading = false;
        this.loadingFile = { show: true, message: 'File uploaded successfully', class: 'alert alert-success', persist: true };
        this.getAttachments();
      }, (resp) => {
        const content = errorHandler(resp);
        this.loadingFile = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
      });
    }
  }

  deleteFile(path) {
    this.deletingFile = '';
    const folder = path.split('/');
    const file = folder.pop();
    this.deleting = true;
    $('#deleteModal').modal('show');
    this.companyAPI.deleteFile(this.company.code, file).subscribe(resp => {
      this.deleting = false;
      this.deletingFile = { show: true, message: 'File deleted', class: 'alert alert-success', persist: true };
      this.getAttachments();
    }, (resp) => {
      this.deleting = false;
      const content = errorHandler(resp);
      this.deletingFile = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  setBirthdate(date) {
    return moment(date).add(1, 'days').format('YYYY-MM-DD');
  }

}
