import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/providers/company.service';
import { Company } from '../interfaces/company.interface';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-blacklist',
  templateUrl: './blacklist.component.html',
  styles: []
})
export class BlacklistComponent implements OnInit {
  loading = true;
  info;
  companies = [];
  constructor(private companyAPI: CompanyService) { }

  ngOnInit() {
    this.getBlackList();
  }

  getBlackList() {
      this.loading = true;
      this.companyAPI.getBlackList()
      .subscribe((resp: Company[]) => {
        this.loading = false;
        this.companies = resp;
      }, (resp) => {
        this.loading = false;
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      });
  }

}
