import { Member } from './member.interface';
import { Loan } from './loan.interface';
import { Statement } from '@angular/compiler';

export interface Company {
    code: string;
    name: string;
    dba: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    phone: string[];
    email: string[];
    service: string;
    members: Member[];
    taxid: string;
    insertBy: any;
    active: boolean;
    createdOn: Date;
    isSended: boolean;
    cardprocessor: boolean;
    loans: Loan[];
    tmpFolder: string[];
    bankStatements: Statement[];
    cardStatements: Statement[];
    additionalAccounts: Statement[];
    app: string;
    _id: string;
    blacklist?: boolean;
}
