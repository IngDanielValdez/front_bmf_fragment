enum PaymentTypes { daily = 'daily', weekly = 'weekly', monthly = 'monthly' }

export interface Loan {
    amount: number;
    currentdebt: number;
    recurrentpayment: number;
    paymenttype: PaymentTypes;
}