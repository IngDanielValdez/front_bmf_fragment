export interface Statement {
    name: string;
    quantity: number;
    period: string;
    createdOn: Date;
}
