export interface Member {
    name: string;
    title: string;
    ownership: number;
    address: string;
    city: string;
    state: string;
    zip: string;
    ssn: string;
    birthdate: Date;
    phone: string[];
    email: string[];
}