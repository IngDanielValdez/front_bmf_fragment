import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { EditComponent } from './components/edit/edit.component';
import { DetailComponent } from './components/detail/detail.component';
import { AuthGuard } from '../auth/auth.guard';
import { AdminGuard } from '../auth/admin.guard';

export const reportsRoutes: Routes = [
    { path: 'reports', component: AppComponent, canActivate: [AuthGuard, AdminGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'edit/:id', component: EditComponent  },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
