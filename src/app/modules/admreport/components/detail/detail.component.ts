import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReportsService } from 'src/app/providers/reports.service';
import { errorHandler } from 'src/app/utils';
declare let $: any;
@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: ['']
})
export class DetailComponent implements OnInit {
  info;
  report;
  loading = true;
  deleting = false;
  closing = false;

  constructor(private reportAPI: ReportsService, private activeRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getReport(params.id));
  }

  getReport(id) {
    this.reportAPI.getReport(id).subscribe((report: any) => {
      this.loading = false;
      this.report = report;
      console.log(report);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  closeReport() {
    this.closing = true;
    this.reportAPI.closeReport(this.report._id).subscribe((resp) => {
      this.closing = false;
      this.report = resp;
      this.info = { show: true, message: 'Commissions have been sent to agents', class: 'alert alert-info', persis: false };
    }, (resp) => {
      this.closing = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  deleteReport() {
    this.deleting = true;
    this.reportAPI.deleteReport(this.report._id).subscribe((resp) => {
      $('#deleteModal').modal('hide');
      this.router.navigate(['/reports']);
    }, (resp) => {
      $('#deleteModal').modal('hide');
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  showTotal() {
    // tslint:disable-next-line:max-line-length
    return this.report.participants.reduce((total, current) => current.type === 'expense' ? total -= current.amount : total += current.amount, 0);
  }

}
