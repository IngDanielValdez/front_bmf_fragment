import { Component, OnInit } from '@angular/core';
import { ReportsService } from 'src/app/providers/reports.service';
import { errorHandler } from 'src/app/utils';
import { Params, ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: ['']
})
export class AllComponent implements OnInit {
  reports = [];
  filters = { skip: 0, limit: 10 };
  info;
  total;
  loading = true;
  constructor(private reportAPI: ReportsService, private router: Router, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getReports();
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getReports();
  }

  getReports() {
    this.loading = true;
    this.reportAPI.getReports(this.filters).subscribe((resp: any) => {
      this.loading = false;
      this.reports = resp.reports;
      console.log(this.reports);
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

}
