import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { UsersService } from 'src/app/providers/users.service';
import { User } from 'src/app/modules/users/interfaces/user.interface';
import { errorHandler } from 'src/app/utils';
import { ReportsService } from 'src/app/providers/reports.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: ['']
})
export class EditComponent implements OnInit {
  reportForm: FormGroup;
  loading = true;
  sending = false;
  report;
  users: User[] = [];
  info;
  closingAmount = 0;
  constructor(private fb: FormBuilder, private userAPI: UsersService, private activeRoute: ActivatedRoute,
              private router: Router, private reportAPI: ReportsService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getReport(params.id));
  }

  getReport(id) {
    this.loading = true;
    this.reportAPI.getReport(id).subscribe((report: any) => {
      this.report = report;
      this.closingAmount = report.closingAmount;
      this.reportForm = this.fb.group({
        participants: this.fb.array([], Validators.minLength(1)),
      });
      this.report.participants.forEach(participant => this.addParticipant(participant));
      this.loading = false;
      this.getUsers();
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  updateReport() {
    this.sending = true;
    this.reportAPI.updateReport(this.reportForm.value, this.report._id).subscribe((report: any) => {
      this.sending = false;
      this.router.navigate(['/reports/details', report._id]);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  getUsers() {
    this.userAPI.getUsers().subscribe((users: User[]) => {
      this.users = users;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  addParticipant(participant = { beneficiary: { _id: '' }, concept: 'commision', type: 'income', amount: 0}) {
    (this.reportForm.controls.participants as FormArray).push(
      this.fb.group({
      beneficiary: new FormControl(participant.beneficiary._id, [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
      concept: new FormControl(participant.concept, [Validators.required]),
      type: new FormControl(participant.type, [Validators.required]),
      amount: new FormControl(participant.amount, [Validators.required]),
    }));
  }

  isInvalidInput(input: FormControl) { return input.touched && input.invalid ? true : false; }

  getInput(input) { return this.reportForm.get(input) as FormControl; }

  getArray(input) { return (this.reportForm.get(input) as FormArray).controls ; }

  removeInArray(input, index) {
    if ((this.reportForm.controls[input] as FormArray).controls.length >= 1) {
      (this.reportForm.controls[input] as FormArray).removeAt(index);
    }
  }
}
