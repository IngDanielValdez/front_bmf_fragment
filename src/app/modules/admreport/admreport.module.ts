import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { EditComponent } from './components/edit/edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { reportsRoutes } from './app.routing';
import { MainModule } from 'src/app/main/main.module';

@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent, EditComponent],
  imports: [
    RouterModule.forRoot(reportsRoutes),
    MainModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class AdmreportModule { }
