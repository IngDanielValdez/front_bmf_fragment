import { Component, OnInit } from '@angular/core';
import { Postman } from '../../interfaces/postman.interface';
import { PostmanService } from 'src/app/providers/postman.service';
import { ActivatedRoute } from '@angular/router';
import { Attempt } from '../../interfaces/attempt.interface';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  tab = 'mail';
  mail: Postman;
  info;
  loading = true;
  constructor(private postmanAPI: PostmanService, private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getMail(params.id));
  }

  getMail(id) {
    this.postmanAPI.getMail(id)
    .subscribe((resp: Postman) => {
      this.loading = false;
      this.mail = resp;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  shortName(name) {
    return name.split('/').pop();
  }

  sortByDate(obj: Attempt[]) {
    if (obj) {
      return obj.sort((a, b) => (new Date(b.triedAt) as any) - (new Date(a.triedAt) as any));
    } else {
      return [];
    }
  }

}
