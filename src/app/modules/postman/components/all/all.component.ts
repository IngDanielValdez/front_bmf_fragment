import { Component, OnInit } from '@angular/core';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { PostmanService } from 'src/app/providers/postman.service';
import { Postman } from '../../interfaces/postman.interface';
import { AuthService } from 'src/app/providers/auth.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  tab = 'postman';
  filters = { from: '', to: '', status: '', skip: 0, limit: 50 };
  loading = true;
  info;
  postman: Postman[] = [];
  senderForm: FormGroup;
  total;
  addresses = [];
  sending = false;
  infoSender;
  constructor(private router: Router, private activeRoute: ActivatedRoute, private postmanAPI: PostmanService,
              private auth: AuthService, private fb: FormBuilder) { }

  ngOnInit() {
    this.getPostman();
    this.getSenders();
    this.senderForm = this.fb.group({
      address: new FormControl('', [Validators.required, Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}$/)]),
      password: new FormControl('', Validators.required)
    });
  }

  getPostman() {
    this.loading = true;
    this.postmanAPI.getPostman(this.filters)
    .subscribe((resp: { postman: Postman[], total: number }) => {
      this.loading = false;
      this.postman = resp.postman;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getPostman();
  }

  getSenders() {
    const roles = ['ADMIN', 'SUPERADMIN', 'MANAGER'];
    if (roles.indexOf(this.auth.getUserInfo().user.role) !== -1) {
      this.postmanAPI.getSenders().subscribe((resp: any) => this.addresses = resp);
    }
  }

  createSender() {
    const roles = ['ADMIN', 'SUPERADMIN', 'MANAGER'];
    if (roles.indexOf(this.auth.getUserInfo().user.role) !== -1) {
      this.sending = true;
      this.postmanAPI.addSender(this.senderForm.value).subscribe((resp: any) => {
        this.getSenders();
        this.sending = false;
        this.infoSender = { show: true, message: 'Address added', class: 'alert alert-success', persist: false };
        this.senderForm.reset();
      }, (resp) => {
        this.sending = false;
        const content = errorHandler(resp);
        this.infoSender = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      });
    }
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  getInput(input) {
    return this.senderForm.get(input) as FormControl;
  }
}
