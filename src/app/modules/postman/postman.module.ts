import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { RouterModule } from '@angular/router';
import { postmanRoutes } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainModule } from 'src/app/main/main.module';



@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent],
  imports: [
    CommonModule,
    MainModule,
    RouterModule.forRoot(postmanRoutes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PostmanModule { }
