export interface Attempt {
    triedAt: string;
    succes: boolean;
    reason: string;
}
