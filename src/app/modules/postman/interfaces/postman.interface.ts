import { Attempt } from './attempt.interface';

export interface Postman {
    body: string;
    status: string;
    from?: string[];
    to: string[];
    subject: string;
    attachments: string[];
    sended?: boolean;
    createdOn?: string;
    insertBy: any;
    attempts?: Attempt[];
    sendedTo: string;
}
