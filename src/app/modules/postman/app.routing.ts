import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { AuthGuard } from '../auth/auth.guard';
import { DetailComponent } from './components/detail/detail.component';
import { AdminGuard } from '../auth/admin.guard';

export const postmanRoutes: Routes = [
    { path: 'postman', component: AppComponent, canActivate: [AuthGuard, AdminGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
