import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { EditComponent } from './components/edit/edit.component';
import { RouterModule } from '@angular/router';
import { listRoutes } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MainModule } from 'src/app/main/main.module';
import { CreateComponent } from './components/create/create.component';
import { ContactDetailComponent } from './components/contact-detail/contact-detail.component';
@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent, EditComponent, CreateComponent, ContactDetailComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(listRoutes),
    FormsModule,
    MainModule,
    ReactiveFormsModule,
  ]
})
export class ContactListModule { }
