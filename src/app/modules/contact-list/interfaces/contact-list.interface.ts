import { User } from '../../users/interfaces/user.interface';

export interface ContactList {
    name: string;
    active: boolean;
    createdOn: Date;
    insertBy: User;
    assignedTo: User;
 }
