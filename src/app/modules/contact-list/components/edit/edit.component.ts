import { Component, OnInit } from '@angular/core';
import { ContactsService } from 'src/app/providers/contacts.service';
import { errorHandler } from 'src/app/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactListService } from 'src/app/providers/contact-list.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})
export class EditComponent {
  importedContacts = [];
  contactList;
  info;
  sending = false;
  loading = true;
  constructor(private contactListAPI: ContactListService, private contactAPI: ContactsService, private activeRoute: ActivatedRoute,
              private router: Router) {
    this.getList(this.activeRoute.snapshot.params.id);
  }

  getList(id) {
    this.loading = true;
    this.contactListAPI.getContactList(id)
    .subscribe((resp: any) => {
      this.loading = false;
      this.contactList = resp;
      console.log(resp);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  createContacts() {
    this.sending = true;
    const contacts = this.importedContacts.filter(contact => {
      if ((contact.name && contact.name !== '') || (contact.companyName && contact.companyName !== '')) {
        return contact;
      }
    });
    this.contactAPI.createContactBulk({ contacts })
    .subscribe((resp: any) => {
      this.sending = false;
      this.router.navigate(['/contact-list/details', this.contactList._id]);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changeListener(files: FileList) {
    if (files && files.length > 0) {
      const file: File = files.item(0);
      if(file.name.split('.')[file.name.split('.').length - 1] !== 'csv') {
        this.info = { show: true, message: 'Invalid file type (Only CSV)', class: 'alert alert-danger', persist: false };
        return;
      }
      const reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = (e) => {
        const csv: string = reader.result as string;
        this.csvJSON(csv);
      };
    }
  }


  csvJSON(csv) {
    const lines = csv.split('\n');
    const result = [];
    const headers = lines[0].split(',');
    for (let i = 1; i < lines.length ; i++) {
      const obj = {};
      const currentline = lines[i].split(',');
      for (let j = 0; j < headers.length; j++) {
        obj[headers[j].trim()] = currentline[j];
        // tslint:disable-next-line:no-string-literal
        obj['contactList'] = this.contactList._id;
      }
      result.push(obj);
    }
    console.log(result);
    this.importedContacts = result;
  }

}
