import { Component, OnInit } from '@angular/core';
import { ContactsService } from 'src/app/providers/contacts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent {
  filters = { name: '', active: true, skip: 0, limit: 50, recall: false };
  loading = true;
  total;
  contacts;
  info;
  constructor(private contactAPI: ContactsService, private activeRoute: ActivatedRoute, private router: Router) {
    this.filters.skip = this.activeRoute.snapshot.queryParams.skip ?
    this.activeRoute.snapshot.queryParams.skip : 0;
    this.filters.recall = this.activeRoute.snapshot.queryParams.recall ?
    this.activeRoute.snapshot.queryParams.recall : false;
    this.getContacts();
  }

  getContacts() {
    this.loading = true;
    console.log(this.filters);
    this.contactAPI.getContacts(this.filters, this.activeRoute.snapshot.params.id).subscribe((resp: { contacts: any[], total: number }) => {
      this.loading = false;
      console.log(resp);
      this.contacts = resp.contacts;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams = { skip: this.filters.skip, recall: this.filters.recall };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getContacts();
  }
}
