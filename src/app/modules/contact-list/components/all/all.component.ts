import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { errorHandler } from 'src/app/utils';
import { ContactListService } from 'src/app/providers/contact-list.service';
declare let $: any;
@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent {
  lists = [];
  loading = true;
  deleting = false;
  total;
  info;
  deleteID;
  filters = { name: '', active: true, skip: 0, limit: 20 };

  constructor(private contactListAPI: ContactListService, private activeRoute: ActivatedRoute, private router: Router) {
    this.filters.skip = this.filters.skip === 0 ?
    this.filters.skip : this.activeRoute.snapshot.queryParams.skip;
    this.getLists();
  }

  getLists() {
    this.loading = true;
    this.contactListAPI.getContactLists(this.filters).subscribe((resp: { contactLists: any[], total: number }) => {
      this.loading = false;
      console.log(resp);
      this.lists = resp.contactLists;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getLists();
  }

  deleteList() {
    this.deleting = true;
    this.contactListAPI.deleteContactList(this.deleteID)
    .subscribe((resp) => {
      this.deleting = false;
      $('#deleteModal').modal('hide');
      this.info = { show: true, message: 'Bank successfully removed', class: 'alert alert-success' };
      this.getLists();
    }, (resp) => {
      $('#deleteModal').modal('hide');
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
