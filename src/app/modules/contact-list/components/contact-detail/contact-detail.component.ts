import { Component, OnInit } from '@angular/core';
import { ContactsService } from 'src/app/providers/contacts.service';
import { ActivatedRoute, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
declare let $: any;
@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styles: []
})
export class ContactDetailComponent {
  contact;
  info;
  loading = true;
  updating = false;
  callForm: FormGroup;
  constructor(private contactAPI: ContactsService, private activeRoute: ActivatedRoute,
              private router: Router, private fb: FormBuilder) {
    this.getContact(this.activeRoute.snapshot.params.id);
    this.callForm = this.fb.group({
      note: new FormControl('', [Validators.required, Validators.maxLength(300), Validators.minLength(1)]),
    });
  }

  getContact(id) {
    this.loading = true;
    this.contactAPI.getContact(id).subscribe((resp) => {
      this.loading = false;
      this.contact = resp;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  addCall() {
    this.updating = true;
    this.contactAPI.registerCall(this.callForm.value, this.contact._id).subscribe((resp) => {
      this.updating = false;
      this.contact = resp;
      $('#updateModal').modal('hide');
    }, (resp) => {
      $('#updateModal').modal('hide');
      this.updating = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  isInvalidInput(input: string) {
    return this.callForm.get(input).touched && this.callForm.get(input).invalid ? true : false;
  }

  toggleRecall() {
    this.updating = true;
    this.contactAPI.updateContact({ recall: !this.contact.recall }, this.contact._id).subscribe((resp) => {
      this.updating = false;
      this.contact = resp;
    }, (resp) => {
      this.updating = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
