import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';
import { ContactListService } from 'src/app/providers/contact-list.service';
import { UsersService } from 'src/app/providers/users.service';
import { User } from 'src/app/modules/users/interfaces/user.interface';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styles: []
})
export class CreateComponent {
  contactListForm: FormGroup;
  contactList;
  info;
  sending = false;
  users = [];
  constructor(private fb: FormBuilder, private contactListAPI: ContactListService,
              private route: Router, private userAPI: UsersService) {
    this.contactListForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(30), Validators.minLength(1)]),
      assignedTo: new FormControl('', [Validators.required, Validators.maxLength(24), Validators.minLength(24)]),
    });
    this.getUsers();
  }

  sendContactList() {
    this.sending = true;
    this.contactListAPI.createContactList(this.contactListForm.value)
    .subscribe((resp: any) => {
      this.sending = false;
      this.contactList = resp;
      this.route.navigate(['/contact-list/edit', resp._id]);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  isInvalidInput(input: string) {
    return this.contactListForm.get(input).touched && this.contactListForm.get(input).invalid ? true : false;
  }

  getUsers() {
    this.userAPI.getUsers().subscribe((users: User[]) => {
      this.users = users;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

}
