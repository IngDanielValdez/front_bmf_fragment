import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { EditComponent } from './components/edit/edit.component';
import { CreateComponent } from './components/create/create.component';
import { ContactDetailComponent } from './components/contact-detail/contact-detail.component';

export const listRoutes: Routes = [
    { path: 'contact-list', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'details/:id', component: DetailComponent },
        { path: 'edit/:id', component: EditComponent },
        { path: 'contact/detail/:id', component: ContactDetailComponent },
        { path: 'add', component: CreateComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
