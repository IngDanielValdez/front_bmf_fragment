import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { MainModule } from 'src/app/main/main.module';
import { RouterModule } from '@angular/router';
import { dealsRoutes } from './app.routing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PendingComponent } from './components/pending/pending.component';
import { BlacklistComponent } from '../companies/blacklist/blacklist.component';

@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent, PendingComponent, BlacklistComponent],
  imports: [
    CommonModule,
    MainModule,
    RouterModule.forRoot(dealsRoutes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class DealsModule { }
