import { BankNotify } from './bank-notify.interface';
import { ClosureReport } from './closure-report.interface';
import { DealHistory } from './deal-history.interface';

enum StatusType {
    rejected = 'rejected',
    accepted = 'accepted',
    onhold = 'onhold',
    replied = 'replied',
    notsent = 'notsent',
    approvednotfunded = 'approvednotfunded',
    completed = 'completed',
    pending = 'pending',
}

enum Territories { USA = 'USA', CANADA = 'CANADA', PR = 'PR', UK = 'UK' }

export interface Deal  {
    companyCode: string;
    companyID: any;
    source?: string;
    code: string;
    banksIDs: BankNotify[];
    quantity: number;
    period: string;
    status: StatusType;
    closingHistory: ClosureReport[];
    lifeHistory: DealHistory[];
    active: boolean;
    createdOn: Date;
    insertBy: any;
    lastSubmitted: Date;
    isRenovation: boolean;
    submittedBy: any;
    territories: Territories[];
    _id: string;
}
