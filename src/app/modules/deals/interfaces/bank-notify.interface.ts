enum StatusType { rejected = 'rejected', accepted = 'accepted', onhold = 'onhold', replied = 'replied' }

export interface BankNotify {
    bankID: string;
    notify: boolean;
    status: StatusType;
    approvedAmount: number;
    addedDate: Date;
    lastNotify: Date;
}
