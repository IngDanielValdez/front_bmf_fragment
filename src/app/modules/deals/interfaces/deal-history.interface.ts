export enum StatusType {
    notsent = 'notsent',
    rejected = 'rejected',
    accepted = 'accepted',
    onhold = 'onhold',
    replied = 'replied',
    approvednotfunded = 'approvednotfunded',
    completed = 'completed',
}

export interface DealHistory {
    status: StatusType;
    statusReason: string;
    dateChange: string;
    insertBy: string;
}
