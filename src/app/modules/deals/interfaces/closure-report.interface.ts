export interface ClosureReport {
    bankID: string;
    closingDate: Date;
    userID: string;
    commission: number;
    psf: number;
    closingAmount: number;
}
