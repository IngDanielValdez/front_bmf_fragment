import { Component, OnInit } from '@angular/core';
import { Deal } from '../../interfaces/deal.interface';
import { DealService } from 'src/app/providers/deal.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { BankService } from 'src/app/providers/bank.service';
import { Bank } from 'src/app/modules/banks/interfaces/bank.interface';
import { DealHistory } from '../../interfaces/deal-history.interface';
import { CalendarService } from 'src/app/providers/calendar.service';
import { UsersService } from 'src/app/providers/users.service';
import { User } from 'src/app/modules/users/interfaces/user.interface';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  tab = 'deal';
  deal: Deal;
  loading = true;
  changing = false;
  changingStatus = false;
  info;
  infoBank;
  infoDeal;
  higherOffer;
  selectedBank = { status: 'onhold', approvedAmount: 0, _id: '' };
  changeForm: FormGroup;
  addBanks: FormGroup;
  closure: FormGroup;
  taskForm: FormGroup;
  selectedBanks;
  banks;
  loadingBanks = false;
  sendingBank = false;
  closing = false;
  creatingTask = false;
  infoClosing;
  infoBanks;
  infoCreatingTask;
  sendBankInfo;
  users = [];
  filters = { name: '', active: true, skip: 0, limit: 20, classification: '', territories: '', exclude: [] };
  constructor(private dealAPI: DealService, private activeRoute: ActivatedRoute, private fb: FormBuilder, private router: Router,
              private bankAPI: BankService, private calendarAPI: CalendarService, private userAPI: UsersService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => this.getDeal(params.id));
    this.changeForm = this.fb.group({
      status: new FormControl('rejected', [Validators.required]),
      message: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(1000)])
    });

    this.addBanks = this.fb.group({
      banksIDs: this.fb.array([]),
      message: new FormControl('', [Validators.required, Validators.maxLength(100)])
    });

    this.closure = this.fb.group({
      bankID: new FormControl('', [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
      closingAmount: new FormControl('', [Validators.required, Validators.min(0)]),
    });

    this.taskForm = this.fb.group({
      title: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(120)]),
      description: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(800)]),
      assignTo: new FormControl('', [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
      reminderDate: new FormControl('', [Validators.required]),
      reference: new FormControl('', [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
    });

    this.userAPI.getUsers().subscribe((resp: User[]) =>  this.users = resp);
  }

  getDeal(id) {
    this.loading = true;
    this.dealAPI.getDeal(id).subscribe((deal: Deal) => {
      if (deal.status === 'pending') { this.router.navigate(['/deals/pending', deal._id]); }
      this.loading = false;
      this.deal = deal;
      this.getHiger();
      this.getBanks();
      this.taskForm.get('reference').setValue(this.deal._id);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  resend() {
    this.sendingBank = true;
    this.dealAPI.resendDeal(this.deal._id, this.addBanks.value).subscribe((resp: any) => {
      this.deal = resp.deal;
      this.sendingBank = false;
      this.addBanks.reset();
      this.sendBankInfo = { show: true, message: 'Message sent', class: 'alert alert-success', persist: false };
      this.getHiger();
      this.getBanks();
    }, (resp) => {
      this.sendingBank = false;
      const content = errorHandler(resp);
      this.sendBankInfo = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  getHiger() {
    if (Array.isArray(this.deal.banksIDs) && this.deal.banksIDs.length > 0) {
      this.higherOffer = this.deal.banksIDs.reduce((total, bank) => {
        return bank.approvedAmount >= total.approvedAmount && bank.status !== 'rejected' ? bank : total;
      });
    }
  }

  getInput(input) {
    return this.changeForm.get(input) as FormControl;
  }

  getInputBanks(input) {
    return this.addBanks.get(input) as FormControl;
  }

  getInputTask(input) {
    return this.taskForm.get(input) as FormControl;
  }

  changeStatus() {
    this.changing = true;
    const update = { notifyID: this.selectedBank._id, status: this.selectedBank.status, approvedAmount: this.selectedBank.approvedAmount };
    this.dealAPI.changeBankStatus(update as any)
    .subscribe((resp: any) => {
      $('#changeBankStatusModal').modal('hide');
      this.changing = false;
      this.deal = resp;
      this.changeForm.reset();
      this.getHiger();
    }, (resp) => {
      this.changing = false;
      const content = errorHandler(resp);
      this.infoBank = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  changeDeal() {
    this.changingStatus = true;
    this.dealAPI.changeStatus(this.deal._id, this.changeForm.value)
    .subscribe((resp: any) => {
      this.deal = resp.deal;
      $('#changeDealStatusModal').modal('hide');
      this.changingStatus = false;
    }, (resp) => {
      this.changingStatus = false;
      const content = errorHandler(resp);
      this.infoDeal = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  statusClass(status) {
    switch (status) {
      case 'onhold': { return 'box-onhold'; }
      case 'rejected': { return 'box-rejected'; }
      case 'accepted': { return 'box-success'; }
      default: { return 'box-replied'; }
   }
  }

  changeClasification(classification) {
    if (this.filters.classification.indexOf(classification + ',') < 0) {
      this.filters.classification = this.filters.classification + `${ classification },`;
    } else {
      this.filters.classification = this.filters.classification.replace(classification + ',', '');
    }
    this.getBanks();
  }

  changeTerritory(territory) {
    if (this.filters.territories.indexOf(territory + ',') < 0) {
      this.filters.territories = this.filters.territories + `${ territory },`;
    } else {
      this.filters.territories = this.filters.territories.replace(territory + ',', '');
    }

    this.getBanks();
  }

  getBanks() {
    this.loadingBanks = true;
    this.bankAPI.getBanks(this.filters).subscribe((resp: { banks: Bank[], total: number }) => {
      this.loadingBanks = false;
      this.banks = resp.banks;
    }, (resp) => {
      this.loadingBanks = false;
      const content = errorHandler(resp);
      this.infoBanks = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  addToselectedBank(bank) {
    const form = (this.addBanks.get('banksIDs') as FormArray);
    const pos = form.value.indexOf(bank._id);
    pos === -1 ? form.push(new FormControl(bank._id)) : form.removeAt(pos);
  }

  inArray(bank) {
    const form = (this.addBanks.get('banksIDs') as FormArray);
    const pos = form.value.indexOf(bank._id);
    return pos === -1 ? false : true;
  }

  sortByDate(obj: DealHistory[]) {
    if (obj) {
      return obj.sort((a, b) => (new Date(b.dateChange) as any) - (new Date(a.dateChange) as any));
    } else {
      return [];
    }
  }

  setBank(bank) {
    this.selectedBank._id = bank._id;
    this.selectedBank.approvedAmount = bank.approvedAmount;
    this.selectedBank.status = bank.status;
  }

  closeDeal() {
    this.closing = true;
    this.dealAPI.closeDeal(this.deal._id, this.closure.value)
    .subscribe((resp: any) => {
      this.deal = resp;
      this.closing = false;
      this.getHiger();
      this.getBanks();
      this.infoClosing = { show: true, message: 'Deal Closed', class: 'alert alert-success', persist: false };
    }, (resp) => {
      this.closing = false;
      const content = errorHandler(resp);
      this.infoClosing = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  createTask() {
    this.creatingTask = true;
    const task = this.taskForm.value;
    task.reminderDate = new Date(task.reminderDate).toISOString();
    this.calendarAPI.createTask(task)
    .subscribe((resp) => {
      this.creatingTask = false;
      this.infoCreatingTask = { show: true, message: 'Task created successfully', class: 'alert alert-success', persist: false };
    }, (resp) => {
      this.creatingTask = false;
      const content = errorHandler(resp);
      this.infoCreatingTask = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

}
