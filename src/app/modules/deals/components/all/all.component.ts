import { Component, OnInit } from '@angular/core';
import { Deal } from '../../interfaces/deal.interface';
import { DealService } from 'src/app/providers/deal.service';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { UsersService } from 'src/app/providers/users.service';
import { User } from 'src/app/modules/users/interfaces/user.interface';
import { errorHandler } from 'src/app/utils';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { AuthService } from 'src/app/providers/auth.service';
const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xlsx';
@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  deals: Deal[] = [];
  loading = true;
  total;
  totalCount = 0;
  info;
  users: User[] = [];
  status = '';
  filters = { status: '', company: '', from: '', to: '', companyCode: '', skip: 0, limit: 10, state: '', submittedBy: '' };
  constructor(private dealAPI: DealService, private router: Router, private activeRoute: ActivatedRoute, private userAPI: UsersService,
              public auth: AuthService) { }

  ngOnInit() {
    this.status = this.activeRoute.snapshot.url[0].path;
    if (this.status === 'pending') { this.filters.status = 'pending'; }
    this.getDeals();
    this.getUsers();
  }

  getDeals() {
    this.loading = true;
    this.dealAPI.getDeals(this.filters).subscribe((resp: { deals: Deal[], total: number }) => {
      this.loading = false;
      this.deals = resp.deals;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
      this.totalCount = resp.total;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getDeals();
  }

  getUsers() {
    this.userAPI.getUsers().subscribe((users: User[]) => {
      this.loading = false;
      this.users = users;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  print() {
    const filters = this.filters;
    filters.limit = 100;
    filters.skip = 0;
    const data = [];
    this.dealAPI.exportDeals(filters).subscribe((resp: Deal[]) => {
      const deals = resp;
      let final =  '';
      deals.forEach((deal) => {
          const date = deal.lastSubmitted || deal.createdOn;
          let banks = '';
          if (deal.banksIDs.length > 0) {
            deal.banksIDs.forEach((bank) => {
            if (bank.status === 'accepted' && bank.bankID && bank.hasOwnProperty('bankID')) {
                banks += `${ (bank.bankID as any).name }: ${ bank.approvedAmount } - `;
              }
            });
          }
          if (deal.status === 'completed' && deal.closingHistory.length > 0) {
            if (deal.closingHistory.length > 0) {
              const dealTPM = deal.closingHistory[deal.closingHistory.length - 1];
              final = `${ (dealTPM.bankID as any).name }: ${ dealTPM.closingAmount}`;
            }
          }
          data.push({
            Company: deal.companyID.name,
            Agent: deal.submittedBy ? deal.submittedBy.name : deal.insertBy.name,
            Bestoffers: banks, FinalOffer: final,
            Status: deal.status, Date: moment(date).format('DD-MM-YYYY'),
          });
    });
      this.exportAsExcelFile(data, 'export');
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  private exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    console.log('worksheet', worksheet);
    const workbook: XLSX.WorkBook = { Sheets: { data: worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], {
      type: EXCEL_TYPE
    });
    FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXCEL_EXTENSION);
  }

  authRole(role: string[]) {
    return role.indexOf(this.auth.getUserInfo().user.role) !== -1 ? true : false;
  }

}
