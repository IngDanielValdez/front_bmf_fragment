import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DealService } from 'src/app/providers/deal.service';
import { CompanyService } from 'src/app/providers/company.service';
import { errorHandler } from 'src/app/utils';
import { Company } from 'src/app/modules/companies/interfaces/company.interface';
import { DealHistory } from '../../interfaces/deal-history.interface';
import { Statement } from 'src/app/modules/companies/interfaces/statement.interface';
import { BanklistsService } from 'src/app/providers/banklists.service';
import { FormBuilder, FormControl, Validators, FormGroup, FormArray } from '@angular/forms';
declare let $: any;

@Component({
  selector: 'app-pending',
  templateUrl: './pending.component.html',
  styles: []
})
export class PendingComponent implements OnInit {
  lists = [];
  deal;
  company;
  loading = true;
  loadingCompany = true;
  loadingDeal = true;
  downloading = false;
  loadingList = false;
  sendingBank = false;
  changingStatus = false;
  addBanks: FormGroup;
  changeForm: FormGroup;
  sendBankInfo;
  infoList;
  infoDeal;
  info;
  infoFile;
  aveBalances = 0;
  period = `${ new Date().getMonth() + 1 }-${new Date().getFullYear() }`;
  totalDeposits = 0;
  constructor(private router: Router, private ActiveRoute: ActivatedRoute, private dealAPI: DealService,
              private companyAPI: CompanyService, private banklistAPI: BanklistsService, private fb: FormBuilder) { }

  ngOnInit() {
    this.loadDeal(this.ActiveRoute.snapshot.params.id);
    this.addBanks = this.fb.group({
      banksIDs: this.fb.array([], [Validators.minLength(1), Validators.required]),
      message: new FormControl('Here you go!!!', [Validators.required, Validators.maxLength(1000)])
    });

    this.changeForm = this.fb.group({
      status: new FormControl('rejected', [Validators.required]),
      message: new FormControl('', [Validators.required, Validators.minLength(5), Validators.maxLength(1000)])
    });
  }

  sendDeal() {
    this.sendingBank = true;
    this.dealAPI.resendDeal(this.deal._id, this.addBanks.value).subscribe((resp: any) => {
      this.deal = resp.deal;
      this.sendingBank = false;
      this.router.navigate(['/deals/details', this.deal._id]);
    }, (resp) => {
      this.sendingBank = false;
      const content = errorHandler(resp);
      this.sendBankInfo = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  selectAll(list) {
    const form = (this.addBanks.get('banksIDs') as FormArray);
    list.banksIDs.forEach(bank => {
      const pos = form.value.indexOf(bank._id);
      if (pos === -1) { form.push(new FormControl(bank._id)); }
    });
  }

  inArray(bank) {
    const form = (this.addBanks.get('banksIDs') as FormArray);
    const pos = form.value.indexOf(bank._id);
    return pos === -1 ? false : true;
  }

  loadLists() {
    this.loadingList = true;
    this.banklistAPI.getLists().subscribe((lists: any[]) => {
      this.lists = lists;
      this.lists.forEach(list => {
        list.banksIDs = list.banksIDs.filter(bank => bank.territories.includes(this.deal.territories[0]));
      });
      this.loadingList = false;
    }, (resp) => {
      this.loadingList = false;
      const content = errorHandler(resp);
      this.infoList = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  loadDeal(id) {
    this.dealAPI.getDeal(id).subscribe((deal: any) => {
      if (deal.status !== 'pending') { this.router.navigate(['/deals/details', deal._id]); }
      this.deal = deal;
      this.loadingDeal = false;
      this.loadLists();
      this.getCompany(deal.companyID._id);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  getCompany(id) {
    this.companyAPI.getCompany(id).subscribe((company: Company) => {
      this.loading = false;
      this.company = company;
      this.loadingCompany = false;
      if (this.company.bankStatements) {
        this.company.bankStatements.map(statement => {
          if (statement.period === this.period) {
            this.aveBalances += statement.quantity;
            this.totalDeposits += statement.quantity;
          }
        });
      }

      if (this.company.additionalAccounts) {
        this.company.additionalAccounts.map(statement => {
          if (statement.period === this.period) {
            this.aveBalances += statement.quantity;
            this.totalDeposits += statement.quantity;
          }
        });
      }

      if (this.company.cardStatements) {
        this.company.cardStatements.map(statement => {
          if (statement.period === this.period) { this.totalDeposits += statement.quantity; }
        });
      }

    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  sortByDate(obj: DealHistory[]) {
    if (obj) {
      return obj.sort((a, b) => (new Date(b.dateChange) as any) - (new Date(a.dateChange) as any));
    } else {
      return [];
    }
  }

  filterStatement( statements: Statement[]) {
    return statements.filter(statement => statement.period === this.period);
  }

  shortName(name) {
    return name.split('/').pop();
  }

  downloadFile(path) {
    this.infoFile = '';
    const folder = path.split('/');
    const file = folder.pop();
    this.downloading = true;
    $('#loadingModal').modal('show');
    this.companyAPI.downloadFile(folder.join('/'), file).subscribe(resp => {
      const url = window.URL.createObjectURL(resp);
      const newWindow = window.open(url);
      this.downloading = false;
      let message = 'Download complete, the file will open in a new tab';
      if (!newWindow || newWindow.closed || typeof newWindow.closed === 'undefined') {
        message = 'Please disable your Pop-up blocker and try again';
      }
      this.infoFile = { show: true, message, class: 'alert alert-success', persist: true };
    }, (resp) => {
      this.downloading = false;
      const content = errorHandler(resp);
      this.infoFile = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  addToselectedBank(bank) {
    const form = (this.addBanks.get('banksIDs') as FormArray);
    const pos = form.value.indexOf(bank._id);
    pos === -1 ? form.push(new FormControl(bank._id)) : form.removeAt(pos);
  }

  changeDeal() {
    this.changingStatus = true;
    this.dealAPI.changeStatus(this.deal._id, this.changeForm.value)
    .subscribe((resp: any) => {
      this.deal = resp.deal;
      $('#changeDealStatusModal').modal('hide');
      this.changingStatus = false;
      this.router.navigate(['/deals/details', this.deal._id]);
    }, (resp) => {
      this.changingStatus = false;
      const content = errorHandler(resp);
      this.infoDeal = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

}
