import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { AdminGuard } from '../auth/admin.guard';
import { PendingComponent } from './components/pending/pending.component';

export const dealsRoutes: Routes = [
    { path: 'deals', component: AppComponent, canActivate: [AuthGuard, AdminGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'pending', component: AllComponent },
        { path: 'details/:id', component: DetailComponent },
        { path: 'pending/:id', component: PendingComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
