import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all.component';
import { CreateComponent } from './components/create.component';
import { EditComponent } from './components/edit.component';
import { DetailComponent } from './components/detail.component';
import { AuthGuard } from '../auth/auth.guard';
import { AdminGuard } from '../auth/admin.guard';

export const banksRoutes: Routes = [
    { path: 'banks', component: AppComponent, canActivate: [AuthGuard, AdminGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'add', component: CreateComponent },
        { path: 'edit/:id', component: EditComponent  },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
