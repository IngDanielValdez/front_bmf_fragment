import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all.component';
import { EditComponent } from './components/edit.component';
import { CreateComponent } from './components/create.component';
import { DetailComponent } from './components/detail.component';
import { RouterModule } from '@angular/router';
import { banksRoutes } from './app.routing';
import { MainModule } from 'src/app/main/main.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [AppComponent, AllComponent, EditComponent, CreateComponent, DetailComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(banksRoutes),
    MainModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class BanksModule { }
