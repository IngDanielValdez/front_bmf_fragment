export interface Bank {
    name: string;
    acronym: string;
    contactName: string;
    address: string;
    city: string;
    state: string;
    zip: string;
    phone: string[];
    email: string[];
    territories: string[];
    classification: string[];
    insertBy: any;
    active: boolean;
    moreThan10k: boolean;
    lessThan10k: boolean;
    createdOn: Date;
    _id: string;
}
