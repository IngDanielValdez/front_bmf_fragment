import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BankService } from 'src/app/providers/bank.service';
import { Bank } from '../interfaces/bank.interface';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  bank: Bank;
  loading = true;
  info;
  deleting = false;
  constructor(private activeRoute: ActivatedRoute, private router: Router, private bankAPI: BankService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getBank(params.id));
  }

  getBank(id) {
    this.bankAPI.getBank(id).subscribe((bank: Bank) => {
      this.loading = false;
      this.bank = bank;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  deleteBank() {
    this.deleting = true;
    this.bankAPI.deleteBank(this.bank._id).subscribe((resp) => {
      $('#deleteModal').modal('hide');
      this.router.navigate(['/banks']);
    }, (resp) => {
      $('#deleteModal').modal('hide');
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
