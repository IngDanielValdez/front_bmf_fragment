import { Component, OnInit } from '@angular/core';
import { Bank } from '../interfaces/bank.interface';
import { BankService } from 'src/app/providers/bank.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  banks: Bank[] = [];
  loading = true;
  total;
  totalbanks = 0;
  deleteID;
  deleting = false;
  info;
  filters = { name: '', contactName: '', active: true, skip: 0, limit: 10, classification: '', territories: '' };

  constructor(private bankAPI: BankService, private activeRoute: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.activeRoute.queryParams.subscribe((query) => {
      this.filters.skip = Number(query.skip) || 0;
      this.getBanks();
    });
  }

  getBanks() {
    this.loading = true;
    this.bankAPI.getBanks(this.filters).subscribe((resp: { banks: Bank[], total: number }) => {
      this.loading = false;
      this.banks = resp.banks;
      this.totalbanks = resp.total;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changeClasification(classification) {
    if (this.filters.classification.indexOf(classification + ',') < 0) {
      this.filters.classification = this.filters.classification + `${ classification },`;
    } else {
      this.filters.classification = this.filters.classification.replace(classification + ',', '');
    }
    this.getBanks();
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getBanks();
  }

  changeTerritory(territory) {
    if (this.filters.territories.indexOf(territory + ',') < 0) {
      this.filters.territories = this.filters.territories + `${ territory },`;
    } else {
      this.filters.territories = this.filters.territories.replace(territory + ',', '');
    }

    this.getBanks();
  }

  deleteBank() {
    this.deleting = true;
    this.bankAPI.deleteBank(this.deleteID)
    .subscribe((resp) => {
      this.deleting = false;
      $('#deleteModal').modal('hide');
      this.info = { show: true, message: 'Bank successfully removed', class: 'alert alert-success' };
      this.getBanks();
    }, (resp) => {
      $('#deleteModal').modal('hide');
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
