import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BankService } from 'src/app/providers/bank.service';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { Bank } from '../interfaces/bank.interface';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})
export class EditComponent implements OnInit {
  loading = true;
  bankForm: FormGroup;
  info;
  bank: Bank;
  sending = false;
  constructor(private fb: FormBuilder, private activeRoute: ActivatedRoute, private route: Router, private bankAPI: BankService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => this.getBank(params.id));
  }

  getBank(id) {
    this.bankAPI.getBank(id).subscribe((bank: Bank) => {
      this.loading = false;
      this.bank = bank;
      this.bankForm = this.fb.group({
        name: new FormControl(bank.name, [Validators.required, Validators.maxLength(120), Validators.minLength(3)]),
        acronym: new FormControl(bank.acronym, [Validators.required, Validators.maxLength(20), Validators.minLength(1)]),
        contactName: new FormControl(bank.contactName, [Validators.required, Validators.maxLength(120), Validators.minLength(1)]),
        address: new FormControl(bank.address, [Validators.required, Validators.maxLength(150)]),
        city: new FormControl(bank.city, [Validators.required, Validators.maxLength(150)]),
        state: new FormControl(bank.state, [Validators.required, Validators.maxLength(150)]),
        zip: new FormControl(bank.zip, [Validators.required, Validators.maxLength(9), Validators.pattern(/[0-9]+/)]),
        phone: this.fb.array([]),
        email: this.fb.array([]),
        territories: this.fb.array([]),
        classification: this.fb.array([]),
      });
      const phoneValidators = [Validators.required, Validators.pattern(/[0-9]/)];
      const emailValidators = [Validators.required, Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)];
      this.bank.phone.forEach((phone) => (this.bankForm.controls.phone as FormArray).push(new FormControl(phone, phoneValidators)));
      this.bank.email.forEach((mail) => (this.bankForm.controls.email as FormArray).push(new FormControl(mail, emailValidators)));
      this.bank.classification.forEach((cls) => this.toggleClass(cls, 'classification'));
      this.bank.territories.forEach((territoy) => this.toggleClass(territoy, 'territories'));

    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  sendBank() {
    this.sending = true;
    this.bankAPI.updateBank(this.bankForm.value, this.bank._id)
    .subscribe((resp: Bank) => {
      this.route.navigate(['/banks/details', resp._id]);
      this.sending = false;
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.sending = false;
    });
  }

  increaseArray(input) {
    const validators = [Validators.required];
    if (input === 'email') { validators.push(Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)); }
    if (input === 'phone') { validators.push(Validators.pattern(/[0-9]/)); }
    (this.bankForm.controls[input] as FormArray).push(new FormControl('', validators));
  }

  removeInArray(input, index) {
    if ((this.bankForm.controls[input] as FormArray).controls.length > 1) {
      (this.bankForm.controls[input] as FormArray).removeAt(index);
    }
  }

  isInvalidInput(input: string) {
    return this.bankForm.get(input).touched && this.bankForm.get(input).invalid ? true : false;
  }

  inClass(letter, input) {
    return this.bankForm.get(input).value.indexOf(letter) >= 0 ? true : false;
  }

  toggleClass(letter, input) {
    const classValue = this.bankForm.get(input).value;
    const classArray = this.bankForm.controls[input] as FormArray;
    const pos = classValue.indexOf(letter);
    pos >= 0 ? classArray.removeAt(pos) : classArray.push(new FormControl(letter));
  }

  getArray(input) { return (this.bankForm.get(input) as FormArray).controls ; }

  isInvalidInputInArray(index, input) {
    return (this.bankForm.get(input) as FormArray).controls[index].touched &&
    (this.bankForm.get(input) as FormArray).controls[index].invalid ? true : false;
  }

  get invalidLength() {
  const phones = this.bankForm.get('phone').value;
  const email = this.bankForm.get('email').value;
  const territories = this.bankForm.get('territories').value;
  const classification = this.bankForm.get('classification').value;
  return phones.length <= 0 || email.length <= 0 || territories.length <= 0 || classification.length <= 0 ? true : false;
  }

}
