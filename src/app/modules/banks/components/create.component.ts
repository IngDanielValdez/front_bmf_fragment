import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { BankService } from 'src/app/providers/bank.service';
import { Bank } from '../interfaces/bank.interface';
import { Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styles: []
})
export class CreateComponent implements OnInit {
  bankForm: FormGroup;
  info;
  sending = false;
  constructor(private fb: FormBuilder, private bankAPI: BankService, private route: Router) { }

  ngOnInit() {
    this.bankForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(120), Validators.minLength(3)]),
      acronym: new FormControl('', [Validators.required, Validators.maxLength(20), Validators.minLength(1)]),
      contactName: new FormControl('', [Validators.required, Validators.maxLength(120), Validators.minLength(1)]),
      address: new FormControl('', [Validators.required, Validators.maxLength(150)]),
      city: new FormControl('', [Validators.required, Validators.maxLength(150)]),
      state: new FormControl('', [Validators.required, Validators.maxLength(150)]),
      zip: new FormControl('', [Validators.required, Validators.maxLength(9), Validators.pattern(/[0-9]+/)]),
      phone: this.fb.array([], Validators.minLength(1)),
      email: this.fb.array([], Validators.minLength(1)),
      territories: this.fb.array([], Validators.minLength(1)),
      classification: this.fb.array([], Validators.minLength(1)),
    });
    this.increaseArray('phone');
    this.increaseArray('email');
  }

  increaseArray(input) {
    const validators = [Validators.required];
    if (input === 'email') { validators.push(Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)); }
    if (input === 'phone') { validators.push(Validators.pattern(/[0-9]/)); }
    (this.bankForm.controls[input] as FormArray).push(new FormControl('', validators));
  }

  removeInArray(input, index) {
    if ((this.bankForm.controls[input] as FormArray).controls.length > 1) {
      (this.bankForm.controls[input] as FormArray).removeAt(index);
    }
  }

  sendBank() {
    this.sending = true;
    this.bankAPI.createBank(this.bankForm.value)
    .subscribe((resp: Bank) => {
      this.route.navigate(['/banks/details', resp._id]);
      this.sending = false;
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  isInvalidInput(input: string) {
    return this.bankForm.get(input).touched && this.bankForm.get(input).invalid ? true : false;
  }

  inClass(letter, input) {
    return this.bankForm.get(input).value.indexOf(letter) >= 0 ? true : false;
  }

  toggleClass(letter, input) {
    const classValue = this.bankForm.get(input).value;
    const classArray = this.bankForm.controls[input] as FormArray;
    const pos = classValue.indexOf(letter);
    pos >= 0 ? classArray.removeAt(pos) : classArray.push(new FormControl(letter));
  }

  getArray(input) { return (this.bankForm.get(input) as FormArray).controls ; }

  isInvalidInputInArray(index, input) {
    return (this.bankForm.get(input) as FormArray).controls[index].touched &&
    (this.bankForm.get(input) as FormArray).controls[index].invalid ? true : false;
  }

  get invalidLength() {
  const phones = this.bankForm.get('phone').value;
  const email = this.bankForm.get('email').value;
  const territories = this.bankForm.get('territories').value;
  const classification = this.bankForm.get('classification').value;
  return phones.length <= 0 || email.length <= 0 || territories.length <= 0 || classification.length <= 0 ? true : false;
  }

}
