import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { BanklistsService } from 'src/app/providers/banklists.service';
import { Router } from '@angular/router';
import { Bank } from 'src/app/modules/banks/interfaces/bank.interface';
import { errorHandler } from 'src/app/utils';
import { BankService } from 'src/app/providers/bank.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styles: []
})
export class CreateComponent implements OnInit {
  listForm: FormGroup;
  info;
  sending = false;
  filters = { name: '', active: true, skip: 0, limit: 20, classification: '', territories: '' };
  loadingBanks = true;
  infoBanks;
  banks;
  selected = [];
  constructor(private fb: FormBuilder, private bankListAPI: BanklistsService, private route: Router, private bankAPI: BankService) { }

  ngOnInit() {
    this.listForm = this.fb.group({
      name: new FormControl('', [Validators.required, Validators.maxLength(80), Validators.minLength(1)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(300), Validators.minLength(1)]),
      banksIDs: this.fb.array([], Validators.minLength(1)),
    });
    this.getBanks();
  }

  sendList() {
    this.sending = true;
    this.bankListAPI.createList(this.listForm.value)
    .subscribe((resp: any) => {
      this.sending = false;
      this.route.navigate(['/bankslists/details', resp._id]);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.infoBanks = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || true };
    });
  }

  isInvalidInput(input: string) {
    return this.listForm.get(input).touched && this.listForm.get(input).invalid ? true : false;
  }

  getBanks() {
    this.loadingBanks = true;
    this.bankAPI.getBanks(this.filters).subscribe((resp: { banks: Bank[], total: number }) => {
      this.loadingBanks = false;
      this.banks = resp.banks;
    }, (resp) => {
      this.loadingBanks = false;
      const content = errorHandler(resp);
      this.infoBanks = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || true };
    });
  }

  changeTerritory(territory) {
    if (this.filters.territories.indexOf(territory + ',') < 0) {
      this.filters.territories = this.filters.territories + `${ territory },`;
    } else {
      this.filters.territories = this.filters.territories.replace(territory + ',', '');
    }

    this.getBanks();
  }

  changeClasification(classification) {
    if (this.filters.classification.indexOf(classification + ',') < 0) {
      this.filters.classification = this.filters.classification + `${ classification },`;
    } else {
      this.filters.classification = this.filters.classification.replace(classification + ',', '');
    }
    this.getBanks();
  }

  inArray(bank) {
    const form = (this.listForm.get('banksIDs') as FormArray);
    return form.value.indexOf(bank._id) === -1 ? false : true;
  }

  addToselectedBank(bank) {
    const form = (this.listForm.get('banksIDs') as FormArray);
    const pos = form.value.indexOf(bank._id);
    pos === -1 ? form.push(new FormControl(bank._id)) : form.removeAt(pos);

    const posBank = this.selected.map(tmpBank => tmpBank._id).indexOf(bank._id);
    posBank === -1 ? this.selected.push(bank) : this.selected.splice(posBank, 1);
  }

}
