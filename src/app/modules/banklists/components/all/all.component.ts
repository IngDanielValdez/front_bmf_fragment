import { Component, OnInit } from '@angular/core';
import { BanklistsService } from 'src/app/providers/banklists.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  loading = false;
  info;
  lists = [];
  constructor(private banklistAPI: BanklistsService) { }

  ngOnInit() {
    this.banklistAPI.getLists().subscribe((lists: any[]) => {
      this.lists = lists;
      this.loading = false;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
