import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BanklistsService } from 'src/app/providers/banklists.service';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  list;
  info;
  loading = true;
  deleting = false;
  constructor(private activeRoute: ActivatedRoute, private banklistAPI: BanklistsService, private router: Router) { }

  ngOnInit() {
    this.getList(this.activeRoute.snapshot.params.id);
  }

  getList(id) {
    this.loading = true;
    this.banklistAPI.getList(id).subscribe((list) => {
      this.loading = false;
      this.list = list;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  deleteList() {
    this.deleting = true;
    this.banklistAPI.deleteList(this.list._id).subscribe((list) => {
      this.deleting = false;
      $('#deleteModal').modal('hide');
      this.router.navigate(['/bankslists/all']);
    }, (resp) => {
      this.deleting = false;
      $('#deleteModal').modal('hide');
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
