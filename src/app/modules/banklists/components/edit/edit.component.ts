import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { BanklistsService } from 'src/app/providers/banklists.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Bank } from 'src/app/modules/banks/interfaces/bank.interface';
import { errorHandler } from 'src/app/utils';
import { BankService } from 'src/app/providers/bank.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styles: []
})
export class EditComponent implements OnInit {
  listForm: FormGroup;
  info;
  sending = false;
  loading = false;
  filters = { name: '', active: true, skip: 0, limit: 20, classification: '', territories: '' };
  loadingBanks = true;
  infoBanks;
  banks;
  selected = [];
  list;
  constructor(private fb: FormBuilder, private banklistAPI: BanklistsService, private route: Router, private bankAPI: BankService,
              private activeRoute: ActivatedRoute) { }

  ngOnInit() {
    this.getList(this.activeRoute.snapshot.params.id);
  }

  getList(id) {
    this.loading = true;
    this.banklistAPI.getList(id).subscribe((list: any) => {
      this.loading = false;
      this.list = list;
      this.getBanks();
      this.listForm = this.fb.group({
        name: new FormControl(list.name, [Validators.required, Validators.maxLength(80), Validators.minLength(1)]),
        description: new FormControl(list.description, [Validators.required, Validators.maxLength(300), Validators.minLength(1)]),
        banksIDs: this.fb.array([], Validators.minLength(1)),
      });
      this.list.banksIDs.forEach((bank) => this.addToselectedBank(bank));
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  sendList() {
    this.sending = true;
    this.banklistAPI.updateList(this.listForm.value, this.list._id)
    .subscribe((resp: any) => {
      this.sending = false;
      this.route.navigate(['/bankslists/details', resp._id]);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.infoBanks = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || true };
    });
  }

  isInvalidInput(input: string) {
    return this.listForm.get(input).touched && this.listForm.get(input).invalid ? true : false;
  }

  getBanks() {
    this.loadingBanks = true;
    this.bankAPI.getBanks(this.filters).subscribe((resp: { banks: Bank[], total: number }) => {
      this.loadingBanks = false;
      this.banks = resp.banks;
    }, (resp) => {
      this.loadingBanks = false;
      const content = errorHandler(resp);
      this.infoBanks = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || true };
    });
  }

  changeTerritory(territory) {
    if (this.filters.territories.indexOf(territory + ',') < 0) {
      this.filters.territories = this.filters.territories + `${ territory },`;
    } else {
      this.filters.territories = this.filters.territories.replace(territory + ',', '');
    }

    this.getBanks();
  }

  changeClasification(classification) {
    if (this.filters.classification.indexOf(classification + ',') < 0) {
      this.filters.classification = this.filters.classification + `${ classification },`;
    } else {
      this.filters.classification = this.filters.classification.replace(classification + ',', '');
    }
    this.getBanks();
  }

  inArray(bank) {
    const form = (this.listForm.get('banksIDs') as FormArray);
    return form.value.indexOf(bank._id) === -1 ? false : true;
  }

  addToselectedBank(bank) {
    const form = (this.listForm.get('banksIDs') as FormArray);
    const pos = form.value.indexOf(bank._id);
    pos === -1 ? form.push(new FormControl(bank._id)) : form.removeAt(pos);

    const posBank = this.selected.map(tmpBank => tmpBank._id).indexOf(bank._id);
    posBank === -1 ? this.selected.push(bank) : this.selected.splice(posBank, 1);
  }

}
