import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';
import { AdminGuard } from '../auth/admin.guard';
import { AllComponent } from './components/all/all.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { DetailComponent } from './components/detail/detail.component';

export const listsRoutes: Routes = [
    { path: 'bankslists', component: AppComponent, canActivate: [AuthGuard, AdminGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'add', component: CreateComponent },
        { path: 'edit/:id', component: EditComponent  },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
