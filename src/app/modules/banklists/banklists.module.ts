import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { MainModule } from 'src/app/main/main.module';
import { RouterModule } from '@angular/router';
import { listsRoutes } from './app.routing';
import { DetailComponent } from './components/detail/detail.component';
import { CreateComponent } from './components/create/create.component';
import { EditComponent } from './components/edit/edit.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent, CreateComponent, EditComponent],
  imports: [
    CommonModule,
    MainModule,
    ReactiveFormsModule,
    FormsModule,
    RouterModule.forRoot(listsRoutes),
  ]
})
export class BanklistsModule { }
