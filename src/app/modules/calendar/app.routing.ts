import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { AuthGuard } from '../auth/auth.guard';

export const calendarRoutes: Routes = [
    { path: 'calendar', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
