import { Component, OnInit} from '@angular/core';
import { CalendarService } from 'src/app/providers/calendar.service';
import { CalendarView, CalendarEvent } from 'angular-calendar';
import {  isSameMonth, isSameDay,  } from 'date-fns';
import { Task } from '../../interfaces/task.interface';
import { Subject } from 'rxjs';
import { ChangeDetectionStrategy } from '@angular/core';
import * as moment from 'moment';
import { UsersService } from 'src/app/providers/users.service';
import { User } from 'src/app/modules/users/interfaces/user.interface';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/providers/auth.service';
import { errorHandler } from 'src/app/utils';
declare const $: any;

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: []
})
export class AllComponent implements OnInit {
  viewDate: Date = new Date();
  filters = { assignTo: this.auth.getUserInfo().user._id, from: '', to: '', status: 'incomplete' };
  view: CalendarView = CalendarView.Month;
  activeDayIsOpen = false;
  refresh: Subject<any> = new Subject();
  tasks: Task[] = [];
  taskForm: FormGroup;
  colors = {
    incomplete: { primary: '#00C851', secondary: '#007E33' },
    completed: { primary: '#e0e0e0', secondary: '#bdbdbd' },
  };
  users: User[];
  events: CalendarEvent[] = [];
  selectedTask;
  deleting = false;
  updating = false;
  creating = false;
  info;
  infoCreating;
  constructor(private calendarAPI: CalendarService, private usersAPI: UsersService, private fb: FormBuilder, private auth: AuthService) { }

  ngOnInit() {
    this.taskForm = this.fb.group({
      title: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(120)]),
      description: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(800)]),
      assignTo: new FormControl(this.auth.getUserInfo().user._id,
        [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
      reminderDate: new FormControl('', [Validators.required]),
    });
    this.getTasks();
    const roles = ['ADMIN', 'SUPERADMIN', 'MANAGER', 'SUPPORT'];
    if (roles.indexOf(this.auth.getUserInfo().user.role) !== -1) {
      this.usersAPI.getUsers().subscribe((resp: User[]) =>  {
        this.users = resp;
        this.refresh.next();
      });
    }
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0 ?
      this.activeDayIsOpen = false : this.activeDayIsOpen = true;
      this.viewDate = date;
    }
  }

  getTasks() {
    this.refresh.next();
    this.events = [];
    this.filters.from = moment(this.viewDate).startOf('month').format('YYYY-MM-DD hh:mm');
    this.filters.to = moment(this.viewDate).endOf('month').format('YYYY-MM-DD hh:mm');
    this.tasks = [];
    this.calendarAPI.getTasks(this.filters)
    .subscribe((resp: Task[]) => {
      resp.forEach((task) => {
        const taskToAdd = {
          title: task.title,
          _id: task._id,
          start: new Date(task.reminderDate),
          color: task.status === 'incomplete' ? this.colors.incomplete : this.colors.completed
        };
        this.tasks = resp;
        this.events.push(taskToAdd as any);
        this.refresh.next();
      });
    });
  }

  closeOpenMonthViewDay() {
    this.getTasks();
    this.activeDayIsOpen = false;
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.selectedTask = this.tasks.filter((task) => task._id === (event as any)._id)[0];
    if (this.selectedTask) {
      $('#viewTask').modal('show');
    }
  }

  markAsDone() {
    this.activeDayIsOpen = false;
    this.info = {};
    this.refresh.next();
    this.updating = true;
    this.calendarAPI.maskAsDone(this.selectedTask._id)
    .subscribe((resp) => {
      this.getTasks();
      this.updating = false;
      $('#viewTask').modal('hide');
    }, (resp) => {
      this.updating = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.refresh.next();
    });
  }

  deleteTask() {
    this.activeDayIsOpen = false;
    this.info = {};
    this.refresh.next();
    this.deleting = true;
    this.calendarAPI.deleteTask(this.selectedTask._id)
    .subscribe((resp) => {
      this.getTasks();
      this.deleting = false;
      $('#viewTask').modal('hide');
    }, (resp) => {
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.refresh.next();
    });
  }

  changeFilter(status) {
    // tslint:disable-next-line:no-string-literal
    status === 'all' ? delete this.filters['status'] : this.filters['status'] = status;
    this.getTasks();
    this.activeDayIsOpen = false;
  }

  createTask() {
    this.activeDayIsOpen = false;
    this.infoCreating = {};
    this.refresh.next();
    this.creating = true;
    const task = this.taskForm.value;
    task.reminderDate = new Date(task.reminderDate).toISOString();
    this.calendarAPI.createTask(task)
    .subscribe((resp) => {
      this.getTasks();
      this.creating = false;
      $('#createTask').modal('hide');
    }, (resp) => {
      this.creating = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      this.refresh.next();
    });
  }

  getInput(input) {
    return this.taskForm.get(input) as FormControl;
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  authRole(role: string[]) {
    return role.indexOf(this.auth.getUserInfo().user.role) !== -1 ? true : false;
  }

}
