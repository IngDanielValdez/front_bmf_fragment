export interface Task {
    title: string;
    description: string;
    active: boolean;
    createdOn: string;
    reminderDate: string;
    insertBy: string;
    assignTo: string;
    reference: string;
    status: string;
    _id: string;
}
