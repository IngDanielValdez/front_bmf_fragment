import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainModule } from 'src/app/main/main.module';
import { RouterModule } from '@angular/router';
import { calendarRoutes } from './app.routing';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { CalendarModule as CM, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AppComponent, AllComponent],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    MainModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(calendarRoutes),
    CM.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ]
})
export class CalendarModule { }
