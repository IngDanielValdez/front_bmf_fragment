import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DetailComponent } from './detail/detail.component';
import { AllComponent } from './all/all.component';
import { RouterModule } from '@angular/router';
import { MainModule } from 'src/app/main/main.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { userRoutes } from './app.routing';

@NgModule({
  declarations: [AppComponent, CreateComponent, EditComponent, DetailComponent, AllComponent],
  imports: [
    CommonModule,
    RouterModule.forRoot(userRoutes),
    MainModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UsersModule { }
