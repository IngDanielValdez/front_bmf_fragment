import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AllComponent } from './all/all.component';
import { EditComponent } from './edit/edit.component';
import { CreateComponent } from './create/create.component';
import { DetailComponent } from './detail/detail.component';
import { AuthGuard } from '../auth/auth.guard';
import { AdminGuard } from '../auth/admin.guard';


export const userRoutes: Routes = [
    { path: 'users', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'add', component: CreateComponent, canActivate: [AdminGuard] },
        { path: 'edit/:id', component: EditComponent, canActivate: [AdminGuard] },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
