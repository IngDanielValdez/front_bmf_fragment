import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { UsersService } from 'src/app/providers/users.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styles: []
})
export class CreateComponent implements OnInit {
  userForm: FormGroup;
  send = false;
  info;
  sending;
  constructor(private fb: FormBuilder, private userAPI: UsersService) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      role: new FormControl('USER', [Validators.required]),
      name: new FormControl('', [Validators.required, Validators.maxLength(30), Validators.minLength(3)]),
      email: new FormControl('', [Validators.required, Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}$/)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    });
  }

  isInvalidInput(input: string) {
    return this.userForm.get(input).touched && this.userForm.get(input).invalid ? true : false;
  }

  createUser() {
    this.userAPI.createUser(this.userForm.value)
    .subscribe(() => {
      this.sending = false;
      this.send = true;
      this.info = { show: true, message: 'An activation code has been sent to the email provided',
      class: 'alert alert-success', persist: true };
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
