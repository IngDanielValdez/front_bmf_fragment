import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/providers/users.service';
import { User } from '../interfaces/user.interface';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/providers/auth.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  users: User[] = [];
  loading = true;
  info;
  url = environment.APIURI;
  constructor(private userAPI: UsersService, private auth: AuthService) { }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    this.userAPI.getUsers().subscribe((users: User[]) => {
      this.loading = false;
      this.users = users;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  prepairPhoto(user: User) {
    if (user.photo) {
      const folder = user.photo.split('/');
      const file = folder.pop();
      return `users/file/download/?path=${ folder.join('/') }&file=${ file }&at=${ this.auth.returnToken() }`;
    }
  }

  authRole(role: string[]) {
    return role.indexOf(this.auth.getUserInfo().user.role) !== -1 ? true : false;
  }

}
