export interface User {
    role: string;
    name: string;
    email: string;
    verifiedEmail: boolean;
    password: string;
    active: boolean;
    createdOn: Date;
    uuid: string;
    photo: string;
    user: string;
    _id: string;
}
