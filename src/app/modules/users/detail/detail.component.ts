import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/providers/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../interfaces/user.interface';
import { AuthService } from 'src/app/providers/auth.service';
import { environment } from 'src/environments/environment';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  user;
  info;
  loading = true;
  sending = false;
  deleting = false;
  url = environment.APIURI;
  constructor(private userAPI: UsersService, private activeRoute: ActivatedRoute, private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => this.getUser(params.id));
  }

  getUser(id: string) {
    this.userAPI.getUser(id).subscribe((user: User) => {
      this.user = user;
      this.loading = false;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  deleteUser() {
    this.deleting = true;
    this.userAPI.deleteUser(this.user._id).subscribe((resp) => {
      $('#deleteModal').modal('hide');
      this.router.navigate(['/users']);
    }, (resp) => {
      $('#deleteModal').modal('hide');
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  prepairPhoto(user: User) {
    if (user.photo) {
      const folder = user.photo.split('/');
      const file = folder.pop();
      return `users/file/download/?path=${ folder.join('/') }&file=${ file }&at=${ this.auth.returnToken() }`;
    }
  }

  changePhoto(event) {
    this.sending = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.userAPI.updatePhoto(this.user._id, file).subscribe((resp: { user: User, auth: string }) => {
        this.user = resp.user;
        this.info = { message: 'Image updated successfully', class: 'alert alert-success', persist: false, show: true };
        this.sending = false;
        if (this.auth.getUserInfo().user._id === this.user._id) {
          this.auth.saveToken(resp.auth);
        }
      }, (resp) => {
        this.sending = false;
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      });
    }
  }

}
