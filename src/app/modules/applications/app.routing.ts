import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from '../auth/auth.guard';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';

export const applicationsRoutes: Routes = [
    { path: 'applications', component: AppComponent, canActivate: [AuthGuard], children: [
        { path: 'all', component: AllComponent },
        { path: 'details/:id', component: DetailComponent },
        { path: '**', redirectTo: 'all' },
        { path: '', redirectTo: 'all', pathMatch: 'full' }
    ]
    }
];
