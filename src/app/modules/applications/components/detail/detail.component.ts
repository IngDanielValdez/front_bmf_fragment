import { Component, OnInit } from '@angular/core';
import { CompanyService } from 'src/app/providers/company.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators, FormArray } from '@angular/forms';
import { DealService } from 'src/app/providers/deal.service';
import * as moment from 'moment';
import { errorHandler } from 'src/app/utils';
import { ApplicationsService } from 'src/app/providers/applications.service';
import { Company } from 'src/app/modules/companies/interfaces/company.interface';
import { Statement } from 'src/app/modules/companies/interfaces/statement.interface';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  tab = 'application';
  application;
  loading = true;
  uploading = false;
  deleting = false;
  downloading = false;
  loadingAttachments = false;
  sendingDeal = false;
  info;
  infoFile;
  infoAttachments;
  infoDeal;
  periods: Set<string>;
  attachments = [];
  loadingFile;
  deletingFile;
  uploadFileObserv: any;
  dealForm: FormGroup;
  sources = ['Google', 'Facebook', 'Instagram', 'Linkedin', 'Cold Call', 'Email'];
  constructor(private fb: FormBuilder, private applicationAPI: ApplicationsService,
              private companyAPI: CompanyService, private activeRoute: ActivatedRoute,
              private dealAPI: DealService, private router: Router) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getCompany(params.id));
  }

  getCompany(id) {
    this.loading = true;
    this.applicationAPI.getApplication(id).subscribe((app: Company) => {
      this.application = app;
      this.loading = false;
      const statements = [];
      statements.push(...this.application.bankStatements);
      statements.push(...this.application.cardStatements);
      statements.push(...this.application.additionalAccounts);
      const filterStatement = statements.map(period => period.period);
      this.periods = new Set(filterStatement);
      this.createForm();
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  createForm() {
    this.dealForm = this.fb.group({
      companyID: new FormControl(this.application._id, Validators.required),
      quantity: new FormControl('', [Validators.required, Validators.min(1000)]),
      territories: this.fb.array([]),
      source: new FormControl('', [Validators.maxLength(100), Validators.required]),
     });
  }


  sendDeal() {
    this.sendingDeal = true;
    this.dealAPI.createDeal(this.dealForm.value)
    .subscribe((resp: any) => {
      this.sendingDeal = false;
      const message = 'Deal sent successfully';
      this.infoDeal = { show: true, message, class: 'alert alert-success', persist: true };
    }, (resp) => {
      this.sendingDeal = false;
      const content = errorHandler(resp);
      this.infoDeal = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changeTerritory(territory) {
    const form = (this.dealForm.get('territories') as FormArray);
    const pos = form.value.indexOf(territory);
    pos === -1 ? form.push(new FormControl(territory)) : form.removeAt(pos);
  }

  isInvalidInput(input: FormControl) {
    return input.touched && input.invalid ? true : false;
  }

  getInput(input) {
    return this.dealForm.get(input) as FormControl;
  }

  filterStatement( statements: Statement[], period: string) {
    return statements.filter(statement => statement.period === period);
  }

  shortName(name) {
    return name.split('/').pop();
  }

  downloadFile(path) {
    this.infoFile = '';
    const folder = path.split('/');
    const file = folder.pop();
    this.downloading = true;
    $('#loadingModal').modal('show');
    this.applicationAPI.downloadFile(folder.join('/'), file).subscribe(resp => {
      const url = window.URL.createObjectURL(resp);
      const newWindow = window.open(url);
      this.downloading = false;
      let message = 'Download complete, the file will open in a new tab';
      if (!newWindow || newWindow.closed || typeof newWindow.closed === 'undefined') {
        message = 'Please disable your Pop-up blocker and try again';
      }
      this.infoFile = { show: true, message, class: 'alert alert-success', persist: true };
    }, (resp) => {
      this.downloading = false;
      const content = errorHandler(resp);
      this.infoFile = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  setReferral(value) {
    const referr = this.dealForm.get('source');
    referr.setValue(value);
  }


  setBirthdate(date) {
    return moment(date).add(1, 'days').format('YYYY-MM-DD');
  }

}
