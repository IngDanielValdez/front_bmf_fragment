import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AllComponent } from './components/all/all.component';
import { DetailComponent } from './components/detail/detail.component';
import { RouterModule } from '@angular/router';
import { applicationsRoutes } from './app.routing';
import { MainModule } from 'src/app/main/main.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [AppComponent, AllComponent, DetailComponent],
  imports: [
    CommonModule,
    MainModule,
    RouterModule.forRoot(applicationsRoutes),
    FormsModule,
    ReactiveFormsModule
  ]
})
export class ApplicationsModule { }
