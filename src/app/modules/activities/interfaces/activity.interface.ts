import { User } from '../../users/interfaces/user.interface';

export interface Activity {
    ip: string;
    method: string;
    token: string;
    browser: string;
    priority: number;
    url: string;
    headers: string;
    user: any;
    body: string;
    createdOn: Date;
    finalStatus: string;
}
