import { Component, OnInit } from '@angular/core';
import { Activity } from '../../interfaces/activity.interface';
import { errorHandler } from 'src/app/utils';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { ActivitiesService } from 'src/app/providers/activities.service';

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  filters = { from: '', to: '', status: '', skip: 0, limit: 50 };
  loading = true;
  info;
  activities: Activity[] = [];
  total;
  constructor(private router: Router, private activeRoute: ActivatedRoute, private actAPI: ActivitiesService) { }

  ngOnInit() {
    this.getActivities();
  }

  getActivities() {
    this.loading = true;
    this.actAPI.getActivities(this.filters)
    .subscribe((resp: { activities: Activity[], total: number }) => {
      this.loading = false;
      this.activities = resp.activities;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getActivities();
  }

}
