import { Component, OnInit } from '@angular/core';
import { errorHandler } from 'src/app/utils';
import { ActivitiesService } from 'src/app/providers/activities.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Activity } from '../../interfaces/activity.interface';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  act: Activity;
  loading = true;
  info;
  deleting = false;
  advanced = false;
  constructor(private activeRoute: ActivatedRoute, private actAPI: ActivitiesService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params) => this.getActivity(params.id));
  }

  getActivity(id) {
    this.actAPI.getActivity(id).subscribe((act: Activity) => {
      this.loading = false;
      this.act = act;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
