import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/providers/users.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { User } from 'src/app/modules/users/interfaces/user.interface';
import { FameService } from 'src/app/providers/fame.service';
import { FeaturedEmployee } from '../../interfaces/featured-employee.interface';
import { Params, Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';
import { environment } from 'src/environments/environment';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-all',
  templateUrl: './all.component.html',
  styles: []
})
export class AllComponent implements OnInit {
  users: User[] = [];
  creating = false;
  loading = false;
  loadingEmployees = false;
  employees: FeaturedEmployee[] = [];
  userForm: FormGroup;
  userFilters = { name: '', skip: 0, limit: 10 };
  infoUser;
  creatingInfo;
  infoFame;
  filters = { username: '', from: '', to: '', limit: 12, skip: 0 };
  total;
  url = environment.APIURI;
  constructor(private userAPI: UsersService, private fb: FormBuilder, private fame: FameService,
              private router: Router, private activeRoute: ActivatedRoute, private auth: AuthService) { }

  ngOnInit() {
    this.getEmployees();
    this.userForm = this.fb.group({
      userID: new FormControl('', [Validators.required, Validators.minLength(24), Validators.maxLength(24)]),
      quantity: new FormControl('', [Validators.required, Validators.min(100)])
     });
  }

  prepairPhoto(employee: FeaturedEmployee) {
    if (employee.photo) {
      const folder = employee.photo.split('/');
      const file = folder.pop();
      return `featured-employee/file/download/?path=${ folder.join('/') }&file=${ file }&at=${ this.auth.returnToken() }`;
    }
  }

  getEmployees() {
    this.loadingEmployees = true;
    this.fame.getHallOfFame(this.filters)
    .subscribe((resp: any) => {
      this.employees = resp.employees;
      this.loadingEmployees = false;
      this.total =  Math.ceil(resp.total / this.filters.limit);
      this.total = Array(this.total).fill(0).map((x, i) => i);
    }, (resp) => {
      this.loadingEmployees = false;
      const content = errorHandler(resp);
      this.infoFame = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  insertUserInHall() {
    this.creating = true;
    this.users = [];
    this.fame.insertInHall(this.userForm.value)
    .subscribe((resp) => {
      this.creating = false;
      $('#createModal').modal('hide');
      this.getEmployees();
    }, (resp) => {
      this.creating = false;
      const content = errorHandler(resp);
      this.creatingInfo = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
    });
  }

  searchUser() {
    if (this.userFilters.name.length > 0) {
      this.loading = true;
      this.userAPI.getUsers().subscribe((users: User[]) => {
        this.loading = false;
        this.users = users;
      }, (resp) => {
        this.loading = false;
        const content = errorHandler(resp);
        this.infoUser = { show: true, message: content.message, class: 'alert alert-danger', persist: true };
      });
    } else {
      this.users = [];
    }
  }

  selectUser(user) {
    const userID = this.userForm.get('userID');
    userID.value === user._id ? userID.setValue('') : userID.setValue(user._id);
  }

  inInput(user) {
    return this.userForm.get('userID').value === user._id ? true : false;
  }

  changePage(event) {
    this.filters.skip = event;
    const queryParams: Params = { skip: this.filters.skip };
    this.router.navigate([], { relativeTo: this.activeRoute, queryParams });
    this.getEmployees();
  }

  authRole(role: string[]) {
    return role.indexOf(this.auth.getUserInfo().user.role) !== -1 ? true : false;
  }

}
