import { Component, OnInit } from '@angular/core';
import { FeaturedEmployee } from '../../interfaces/featured-employee.interface';
import { FameService } from 'src/app/providers/fame.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { AuthService } from 'src/app/providers/auth.service';
import { errorHandler } from 'src/app/utils';
declare let $: any;

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styles: []
})
export class DetailComponent implements OnInit {
  employee: FeaturedEmployee;
  loading = true;
  deleting = false;
  updating = false;
  sending = false;
  info;
  infoUpdate;
  quantity;
  url = environment.APIURI;
  constructor(private fame: FameService, private activeRoute: ActivatedRoute,
              private router: Router, private auth: AuthService) { }

  ngOnInit() {
    this.activeRoute.params.subscribe(params => this.getEmployee(params.id));
  }

  getEmployee(id) {
    this.loading = true;
    this.fame.getEmployee(id).subscribe((resp: FeaturedEmployee) => {
      this.employee = resp;
      this.loading = false;
      this.quantity = this.employee.quantity;
    }, (resp) => {
      this.loading = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  prepairPhoto(employee: FeaturedEmployee) {
    if (employee.photo) {
      const folder = employee.photo.split('/');
      const file = folder.pop();
      return `featured-employee/file/download/?path=${ folder.join('/') }&file=${ file }&at=${ this.auth.returnToken() }`;
    }
  }

  deleteEmployee() {
    this.deleting = true;
    this.fame.deleteEmployee(this.employee._id)
    .subscribe((resp) => {
      this.router.navigate(['/halloffame']);
      $('#deleteModal').modal('hide');
    }, (resp) => {
      $('#deleteModal').modal('hide');
      this.deleting = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  updateQuantity() {
    this.updating = true;
    this.fame.updateEmployee(this.employee._id, { quantity: this.quantity })
    .subscribe((resp: FeaturedEmployee) => {
      $('#editModal').modal('hide');
      this.employee = resp;
      this.updating = false;
    }, (resp) => {
      $('#editModal').modal('hide');
      this.updating = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  changePhoto(event) {
    this.sending = true;
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.fame.updatePhoto(this.employee._id, file).subscribe((employee: FeaturedEmployee) => {
        this.employee = employee;
        this.info = { message: 'Image updated successfully', class: 'alert alert-success', persist: false, show: true };
        this.sending = false;
      }, (resp) => {
        this.sending = false;
        const content = errorHandler(resp);
        this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      });
    }
  }

}
