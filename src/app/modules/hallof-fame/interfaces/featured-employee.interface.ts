export interface FeaturedEmployee {
    userID: any;
    insertBy?: any;
    quantity: number;
    photo?: string;
    _id?: string;
    createdOn: string;
}
