import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private auth: AuthService, private router: Router) { }
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    if (this.auth.getUserInfo().user.role === 'ADMIN' ||
        this.auth.getUserInfo().user.role === 'SUPERADMIN' ||
        this.auth.getUserInfo().user.role === 'MANAGER' ||
        this.auth.getUserInfo().user.role === 'SUPPORT') {
      return true;
    } else {
      this.router.navigate(['/dashboard']);
    }
  }
}
