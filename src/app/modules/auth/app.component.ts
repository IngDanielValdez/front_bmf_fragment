import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  template: '<router-outlet></router-outlet>',
  styles: []
})
export class AppComponent {}

