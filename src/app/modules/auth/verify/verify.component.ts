import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { User } from '../../users/interfaces/user.interface';
import { UsersService } from 'src/app/providers/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/providers/auth.service';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styles: []
})
export class VerifyComponent implements OnInit {
  verifyForm: FormGroup;
  sending = false;
  info;
  resend = false;
  constructor(private fb: FormBuilder, private userAPI: UsersService, private activeRoute: ActivatedRoute,
              private auth: AuthService, private router: Router) { }

  ngOnInit() {
    const userID = this.activeRoute.snapshot.params.id;
    this.verifyForm = this.fb.group({
      hash: new FormControl('', [Validators.required]),
      userID: new FormControl(userID, [Validators.required])
    });
  }

  isInvalidInput(input: string) {
    return this.verifyForm.get(input).touched && this.verifyForm.get(input).invalid ? true : false;
  }

  verify() {
    this.sending = true;
    const verify = this.verifyForm.value;
    this.userAPI.verifyUser(verify.userID, verify.hash)
    .subscribe((resp: any) => {
      this.sending = false;
      this.auth.saveToken(resp.auth);
      this.router.navigate(['/dashboard']);
    }, (resp) => {
      this.sending = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

  resendCode() {
    this.resend = true;
    const verify = this.verifyForm.value;
    this.userAPI.resendCode(verify.userID)
    .subscribe((resp: any) => {
      this.resend = false;
      this.info = { show: true, message: 'A new token has been sent', class: 'alert alert-success', persist: false };
    }, (resp) => {
      this.resend = false;
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
    });
  }

}
