import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthService } from 'src/app/providers/auth.service';
import { Router } from '@angular/router';
import { errorHandler } from 'src/app/utils';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  info;
  constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: new FormControl('', [Validators.required, Validators.pattern(/[aA-zZ0-9._%+-]+@[aA-zZ0-9.-]+\.[aA-zZ]{2,6}/)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8)])
    });
  }

  isInvalidInput(input: string) {
    return this.loginForm.get(input).touched && this.loginForm.get(input).invalid ? true : false;
  }

  login() {
    this.loading = true;
    const user = this.loginForm.value;
    user.email = user.email.trim();
    this.auth.login(user).subscribe((data) => {
      this.loading = false;
      this.router.navigateByUrl('/dashboard');
    }, (resp) => {
      const content = errorHandler(resp);
      this.info = { show: true, message: content.message, class: 'alert alert-danger', persist: content.persist || false };
      if (resp.status === 401 && resp.error.error === '0002') {
        this.router.navigate(['/auth/verify', resp.error.message]);
      }
      this.loading = false;
    });
  }

}
