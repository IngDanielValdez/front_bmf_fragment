import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { RecoverComponent } from './recover/recover.component';
import { RouterModule } from '@angular/router';
import { loginRoutes } from './app.routing';
import { ReactiveFormsModule } from '@angular/forms';
import { MainModule } from 'src/app/main/main.module';

@NgModule({
  declarations: [AppComponent, LoginComponent, VerifyComponent, RecoverComponent],
  imports: [
    CommonModule,
    MainModule,
    RouterModule.forRoot(loginRoutes),
    ReactiveFormsModule
  ],
  exports: [AppComponent]
})
export class AuthModule { }
