import { Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { VerifyComponent } from './verify/verify.component';
import { RecoverComponent } from './recover/recover.component';
import { IsActiveGuard } from './is-active.guard';

export const loginRoutes: Routes = [
    { path: 'auth', component: AppComponent, canActivate: [IsActiveGuard], children: [
        { path: 'login', component: LoginComponent },
        { path: 'verify/:id', component: VerifyComponent  },
        { path: 'recover', component: RecoverComponent },
        { path: '**', redirectTo: 'login' },
        { path: '', redirectTo: 'login', pathMatch: 'full' }
    ]
    }
];
