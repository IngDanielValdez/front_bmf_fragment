import { Routes } from '@angular/router';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AuthGuard } from './modules/auth/auth.guard';

export const Routing: Routes = [
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: '**', pathMatch: 'full', redirectTo: 'dashboard'},
];
