import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private token: string;

  constructor(private http: HttpClient, private router: Router, private auth: AuthService) { }

  public saveToken(token: string) {
    localStorage.setItem('authApp', token);
    this.token = token;
  }

  private getToken(): string {
    if (!this.token) {
      return localStorage.getItem('authApp');
    }
    return this.token;
  }

  public logOut() {
    this.token = '';
    localStorage.removeItem('authApp');
    this.router.navigateByUrl('auth/login');
  }

  public getUserInfo() {
    const token = this.getToken();
    if (token) {
      const base64Url = token.split('.')[1];
      const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
      const jsonPayload = decodeURIComponent(atob(base64).split('').map((c) =>  '%' + ('00' + c.charCodeAt(0)
      .toString(16))
      .slice(-2))
      .join(''));
      return JSON.parse(jsonPayload);
    } else {
      return null;
    }
  }

  public isLogged() {
    const user = this.getUserInfo();
    if (user) {
      return user.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

  private request(user: any): Observable<any> {
    const headers = new HttpHeaders({ accept: 'application/json', 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'auth/login', user, { headers }).pipe(map((res: any) => {
      if (res.auth) { this.saveToken(res.auth); }
      return res;
    }));
  }

  public returnToken() {
    if (this.isLogged) {
      return this.getToken();
    } else {
      return null;
    }
  }

  public login(user) {
    return this.request(user);
  }
}
