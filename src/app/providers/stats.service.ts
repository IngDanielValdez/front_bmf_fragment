import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { createFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class StatsService {

  constructor(private http: HttpClient, private auth: AuthService) {}

  getInitStats() {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'stats/', { headers });
  }

}
