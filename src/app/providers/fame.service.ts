import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { createFormData } from '../utils';


@Injectable({
  providedIn: 'root'
})
export class FameService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getHallOfFame(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'featured-employee/?' + query, { headers });
  }

  public getEmployee(id: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'featured-employee/' + id, { headers });
  }

  public insertInHall(options: { quantity: number, userID: string }) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'featured-employee', options, { headers });
  }

  public updateEmployee(id: string, options: { quantity: number }) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'featured-employee/' + id, options, { headers });
  }

  public deleteEmployee(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'featured-employee/' + id, { headers });
  }

  public updatePhoto(id: string, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'featured-employee/' + id + '/upload', upload, { headers });
  }
}
