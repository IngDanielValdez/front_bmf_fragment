import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { createFormData } from '../utils';
import { BankNotify } from '../modules/deals/interfaces/bank-notify.interface';

@Injectable({
  providedIn: 'root'
})
export class DealService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public exportDeals(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'deals/export/?' + query, { headers });
  }

  public getDeals(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'deals/?' + query, { headers });
  }

  public getDeal(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'deals/' + id, { headers });
  }

  public createDeal(deal) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'deals', deal, { headers });
  }

  public changeBankStatus(notify: BankNotify) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'deals/updatenotify', notify, { headers });
  }

  public changeStatus(id: string, status: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'deals/changestatus/' + id, status, { headers });
  }

  public resendDeal(id: string, banks: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'deals/send/' + id, banks, { headers });
  }

  public closeDeal(id: string, closure: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'deals/close/' + id, closure, { headers });
  }
}
