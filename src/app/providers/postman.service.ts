import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { createFormData } from '../utils';

@Injectable({
  providedIn: 'root'
})
export class PostmanService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getPostman(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'postman/?' + query, { headers });
  }

  public getMail(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'postman/' + id, { headers });
  }

  public getSenders() {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'sender', { headers });
  }

  public addSender(sender) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'sender', sender, { headers });
  }
}
