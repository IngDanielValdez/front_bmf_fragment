import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';
import { Bank } from '../modules/banks/interfaces/bank.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BanklistsService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getLists() {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'bank-list/', { headers });
  }

  public getList(id: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'bank-list/' + id, { headers });
  }

  public deleteList(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'bank-list/' + id, { headers });
  }

  public createList(list) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'bank-list', list, { headers });
  }

  public updateList(list, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'bank-list/' + id, list, { headers });
  }
}
