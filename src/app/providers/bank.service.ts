import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';
import { Bank } from '../modules/banks/interfaces/bank.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BankService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getBanks(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'banks/?' + query, { headers });
  }

  public getBank(id: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'banks/' + id, { headers });
  }

  public deleteBank(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'banks/' + id, { headers });
  }

  public createBank(bank) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'banks', bank, { headers });
  }

  public updateBank(bank: Bank, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'banks/' + id, bank, { headers });
  }
}
