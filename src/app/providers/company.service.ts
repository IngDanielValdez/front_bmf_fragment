import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';
import { Company } from '../modules/companies/interfaces/company.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getCompanies(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.get(environment.APIURI + 'companies/?' + query, { headers });
  }

  public getBlackList() {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.get(environment.APIURI + 'companies/blacklist', { headers });
  }

  public getCompany(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.get(environment.APIURI + 'companies/' + id, { headers });
  }

  public updateFile(code: string, folder, file: FormData) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const query = createFormData(folder);
    return this.http.post(environment.APIURI + 'companies/upload/' + code + '/?' + query, file, { headers });
  }

  public generatePreCode() {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'companies/precode/generate', { headers });
  }

  public createCompany(company: Company) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'companies', company, { headers });
  }

  public updateCompany(company: Company, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'companies/' + id, company, { headers });
  }

  public downloadFile(folder, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const name = encodeURIComponent(file);
    return this.http.get(environment.APIURI + 'companies/file/download?path=' + folder + '&file=' + name,
    { headers, responseType: 'blob' as 'json' });
  }

  public getAttachments(code) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'companies/file/attachments/' + code, { headers });
  }

  public deleteFile(code, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'companies/file/attachments/' + code + '?name=' + file, { headers });
  }
}
