import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getReports(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'reports/?' + query, { headers });
  }

  public getReport(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'reports/' + id, { headers });
  }

  public createReport(report) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.post(environment.APIURI + 'reports', report, { headers });
  }

  public updateReport(report, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.put(environment.APIURI + 'reports/' + id, report, { headers });
  }

  public deleteReport(id) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.delete(environment.APIURI + 'reports/' + id, { headers });
  }

  public closeReport(id) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'reports/' + id + '/close', {}, { headers });
  }
}
