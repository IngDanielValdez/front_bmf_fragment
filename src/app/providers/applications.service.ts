import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './auth.service';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationsService {

  constructor(private http: HttpClient, private auth: AuthService) {}

  public getApplications(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.get(environment.APIURI + 'application/?' + query, { headers });
  }

  public getApplication(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.get(environment.APIURI + 'application/' + id, { headers });
  }

  public downloadFile(folder, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const name = encodeURIComponent(file);
    return this.http.get(environment.APIURI + 'application/file/download?path=' + folder + '&file=' + name,
    { headers, responseType: 'blob' as 'json' });
  }
}
