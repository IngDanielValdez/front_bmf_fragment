import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { Task } from '../modules/calendar/interfaces/task.interface';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getTasks(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'calendar/?' + query, { headers });
  }

  public createTask(task: Task) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'calendar', task, { headers });
  }

  public maskAsDone(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.put(environment.APIURI + 'calendar/' + id, {}, { headers });
  }

  public deleteTask(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.delete(environment.APIURI + 'calendar/' + id, { headers });
  }
}
