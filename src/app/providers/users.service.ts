import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment.prod';
import { User } from '../modules/users/interfaces/user.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getUsers() {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'users/?', { headers });
  }

  public getUser(id: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.get(environment.APIURI + 'users/' + id, { headers });
  }

  public deleteUser(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'users/' + id, { headers });
  }

  public createUser(user: User) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'users', user, { headers });
  }

  public updateUser(user: User, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'users/' + id, user, { headers });
  }

  public updatePhoto(id: string, file) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    const upload = new FormData();
    upload.append('file', file, file.name);
    return this.http.put(environment.APIURI + 'users/' + id + '/upload', upload, { headers });
  }

  public verifyUser(userID: string, hash: string) {
    return this.http.get(environment.APIURI + 'auth/' + userID + '/verify/' + hash);
  }

  public resendCode(userID: string) {
    return this.http.get(environment.APIURI + 'auth/' + userID + '/resend');
  }
}
