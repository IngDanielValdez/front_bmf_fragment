import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getContacts(filters: any, contactID) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'contact/' + contactID + '/?' + query, { headers });
  }

  public getContact(id: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'contact/' + id + '/detail', { headers });
  }

  public registerCall(call: any, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.put(environment.APIURI + 'contact/' + id + '/makecall', call, { headers });
  }

  public deleteContact(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'contact/' + id, { headers });
  }

  public createContact(contact) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'contact', contact, { headers });
  }

  public createContactBulk(contacts) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'contact/bulk', contacts, { headers });
  }

  public updateContact(contact, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'contact/' + id, contact, { headers });
  }
}
