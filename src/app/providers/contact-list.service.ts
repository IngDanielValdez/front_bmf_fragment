import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { createFormData } from '../utils';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { ContactList } from '../modules/contact-list/interfaces/contact-list.interface';

@Injectable({
  providedIn: 'root'
})
export class ContactListService {
  constructor(private http: HttpClient, private auth: AuthService) {}

  public getContactLists(filters: any) {
    const query = createFormData(filters);
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'contact-list/?' + query, { headers });
  }

  public getContactList(id: any) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), accept: 'application/json' });
    return this.http.get(environment.APIURI + 'contact-list/' + id, { headers });
  }

  public deleteContactList(id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken() });
    return this.http.delete(environment.APIURI + 'contact-list/' + id, { headers });
  }

  public createContactList(contactList) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.post(environment.APIURI + 'contact-list', contactList, { headers });
  }

  public updateContactList(contactList: ContactList, id: string) {
    const headers = new HttpHeaders({ Authorization: this.auth.returnToken(), 'Content-Type': 'application/json' });
    return this.http.put(environment.APIURI + 'contact-list/' + id, contactList, { headers });
  }
}
